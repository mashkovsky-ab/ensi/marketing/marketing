<?php


namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Data\DiscountStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountStatusesResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 * @mixin DiscountStatus
 */
class DiscountStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
