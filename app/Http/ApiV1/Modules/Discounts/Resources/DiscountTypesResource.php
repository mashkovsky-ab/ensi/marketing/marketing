<?php


namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Data\DiscountType;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountTypesResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 * @mixin DiscountType
 */
class DiscountTypesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
