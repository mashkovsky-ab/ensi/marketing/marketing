<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class CalculatorForCatalogResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 */
class CalculatorForCatalogResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [];
        foreach ($this->resource as $offer) {
            $discounts = [];
            foreach ($offer["discounts"] as $discount) {
                $discounts[] = [
                    "id" => $discount["id"],
                    "change" => $discount["change"],
                    "value" => $discount["value"],
                    "value_type" => $discount["value_type"],
                ];
            }

            $response[] = [
                'offer_id' => $offer["offer_id"],
                'price' => $offer["price"],
                'cost' => $offer["cost"],
                'change' => $offer["change"],
                'discounts' => $discounts,
            ];
        }

        return $response;
    }
}
