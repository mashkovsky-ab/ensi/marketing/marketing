<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\DiscountCategory;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountCategoriesResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 *
 * @mixin DiscountCategory
 */
class DiscountCategoriesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'discount_id' => $this->discount_id,
            'category_id' => $this->category_id,
        ];
    }
}
