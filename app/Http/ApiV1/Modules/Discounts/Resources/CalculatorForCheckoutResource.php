<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class CalculatorForCheckoutResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 */
class CalculatorForCheckoutResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'promoCode' => $this->resource["promoCode"],
            'discounts' => $this->resource["discounts"],
            'offers' => $this->resource["offers"],
            'deliveries' => $this->resource["deliveries"],
        ];
    }
}
