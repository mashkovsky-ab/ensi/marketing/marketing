<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\DiscountCondition;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountConditionsResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 *
 * @mixin DiscountCondition
 */
class DiscountConditionsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'discount_id' => $this->discount_id,
            'type' => $this->type,
            'condition' => $this->condition,
        ];
    }
}
