<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountsResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 *
 * @mixin Discount
 */
class DiscountsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'seller_id' => $this->seller_id,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'name' => $this->name,
            'value_type' => $this->value_type,
            'value' => $this->value,
            'status' => $this->status,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'promo_code_only' => $this->promo_code_only,
            'created_at' => $this->created_at,
            'offers' => DiscountOffersResource::collection($this->whenLoaded('offers')),
            'brands' => DiscountBrandsResource::collection($this->whenLoaded('brands')),
            'categories' => DiscountCategoriesResource::collection($this->whenLoaded('categories')),
            'segments' => DiscountSegmentsResource::collection($this->whenLoaded('segments')),
            'conditions' => DiscountConditionsResource::collection($this->whenLoaded('conditions')),
        ];
    }
}
