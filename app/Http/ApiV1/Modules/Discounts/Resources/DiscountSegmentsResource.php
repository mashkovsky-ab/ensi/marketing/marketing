<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\DiscountSegment;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountSegmentsResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 *
 * @mixin DiscountSegment
 */
class DiscountSegmentsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'discount_id' => $this->discount_id,
            'segment_id' => $this->segment_id,
        ];
    }
}
