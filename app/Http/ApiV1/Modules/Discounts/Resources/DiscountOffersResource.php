<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\DiscountOffer;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountOffersResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 *
 * @mixin DiscountOffer
 */
class DiscountOffersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'discount_id' => $this->discount_id,
            'offer_id' => $this->offer_id,
            'except' => $this->except,
        ];
    }
}
