<?php

namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Models\DiscountBrand;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountBrandsResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 *
 * @mixin DiscountBrand
 */
class DiscountBrandsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'discount_id' => $this->discount_id,
            'brand_id' => $this->brand_id,
            'except' => $this->except,
        ];
    }
}
