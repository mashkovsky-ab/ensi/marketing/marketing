<?php


namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Data\DiscountConditionType;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountConditionTypesResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 * @mixin DiscountConditionType
 */
class DiscountConditionTypesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'props' => $this->props,
        ];
    }
}
