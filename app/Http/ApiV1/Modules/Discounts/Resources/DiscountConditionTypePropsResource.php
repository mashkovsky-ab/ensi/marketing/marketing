<?php


namespace App\Http\ApiV1\Modules\Discounts\Resources;

use App\Domain\Discounts\Data\DiscountConditionTypeProp;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class DiscountConditionTypePropsResource
 * @package App\Http\ApiV1\Modules\Discounts\Resources
 * @mixin DiscountConditionTypeProp
 */
class DiscountConditionTypePropsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
        ];
    }
}
