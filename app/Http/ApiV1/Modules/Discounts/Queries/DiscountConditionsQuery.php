<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\DiscountCondition;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DiscountConditionsQuery
 * @package App\Http\ApiV1\Modules\Discounts\Queries
 */
class DiscountConditionsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = DiscountCondition::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('discount_id'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('condition'),
        ]);

        $this->defaultSort('id');
    }
}
