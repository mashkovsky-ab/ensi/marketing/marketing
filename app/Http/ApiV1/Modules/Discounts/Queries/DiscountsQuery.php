<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\Support\Filters\FiltersAfterStartDate;
use App\Http\ApiV1\Support\Filters\FiltersLike;
use App\Http\ApiV1\Support\Filters\FiltersBeforeEndDate;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DiscountsQuery
 * @package App\Http\ApiV1\Modules\Discounts\Queries
 */
class DiscountsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Discount::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'created_at', 'start_date', 'end_date']);

        $this->allowedIncludes(['offers', 'brands', 'categories', 'segments', 'conditions']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('type'),
            AllowedFilter::exact('value_type'),
            AllowedFilter::exact('value'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('start_date'),
            AllowedFilter::exact('end_date'),
            AllowedFilter::exact('promo_code_only'),
            AllowedFilter::custom('name', new FiltersLike),
            AllowedFilter::custom('start_date_from', new FiltersAfterStartDate),
            AllowedFilter::custom('end_date_to', new FiltersBeforeEndDate),
            AllowedFilter::scope('is_unlimited'),
            AllowedFilter::scope('created_at_from'),
            AllowedFilter::scope('created_at_to'),
        ]);

        $this->defaultSort('id');
    }
}
