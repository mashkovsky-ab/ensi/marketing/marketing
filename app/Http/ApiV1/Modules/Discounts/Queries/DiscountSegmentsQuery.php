<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\DiscountSegment;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DiscountCategoriesQuery
 * @package App\Http\ApiV1\Modules\Discounts\Queries
 */
class DiscountSegmentsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = DiscountSegment::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('discount_id'),
            AllowedFilter::exact('segment_id'),
        ]);

        $this->defaultSort('id');
    }
}
