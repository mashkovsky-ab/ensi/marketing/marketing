<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\DiscountBrand;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DiscountBrandsQuery
 * @package App\Http\ApiV1\Modules\Discounts\Queries
 */
class DiscountBrandsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = DiscountBrand::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('discount_id'),
            AllowedFilter::exact('brand_id'),
            AllowedFilter::exact('except'),
        ]);

        $this->defaultSort('id');
    }
}
