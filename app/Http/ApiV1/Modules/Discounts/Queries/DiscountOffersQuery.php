<?php

namespace App\Http\ApiV1\Modules\Discounts\Queries;

use App\Domain\Discounts\Models\DiscountOffer;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class DiscountOffersQuery
 * @package App\Http\ApiV1\Modules\Discounts\Queries
 */
class DiscountOffersQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = DiscountOffer::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('discount_id'),
            AllowedFilter::exact('offer_id'),
            AllowedFilter::exact('except'),
        ]);

        $this->defaultSort('id');
    }
}
