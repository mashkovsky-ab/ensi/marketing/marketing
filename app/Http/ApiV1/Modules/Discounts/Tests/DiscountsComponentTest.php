<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/discounts/discounts:mass-status-update 400', function () {
    postJson('/api/v1/discounts/discounts:mass-status-update')
        ->assertStatus(400);
});

test('GET /api/v1/discounts/discount-statuses 200', function () {
    getJson('/api/v1/discounts/discount-statuses')
        ->assertStatus(200);
});

test('GET /api/v1/discounts/discount-types 200', function () {
    getJson('/api/v1/discounts/discount-types')
        ->assertStatus(200);
});
