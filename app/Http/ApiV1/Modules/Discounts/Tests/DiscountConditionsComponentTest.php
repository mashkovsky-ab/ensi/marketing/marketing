<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/discounts/discount-condition-types 200', function () {
    getJson('/api/v1/discounts/discount-condition-types')
        ->assertStatus(200);
});

test('GET /api/v1/discounts/discount-condition-type-props 200', function () {
    getJson('/api/v1/discounts/discount-condition-type-props')
        ->assertStatus(200);
});
