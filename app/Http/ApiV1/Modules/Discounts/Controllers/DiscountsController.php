<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\Discount\CreateDiscountAction;
use App\Domain\Discounts\Actions\Discount\DeleteDiscountAction;
use App\Domain\Discounts\Actions\Discount\MassStatusUpdateAction;
use App\Domain\Discounts\Actions\Discount\PatchDiscountAction;
use App\Domain\Discounts\Actions\Discount\ReplaceDiscountAction;
use App\Domain\Discounts\Data\DiscountStatus;
use App\Domain\Discounts\Data\DiscountType;
use App\Domain\Discounts\Services\CalculatorBuilder;
use App\Domain\Discounts\Services\CatalogCalculator;
use App\Domain\Discounts\Services\CheckoutCalculator;
use App\Http\ApiV1\Modules\Discounts\Queries\DiscountsQuery;
use App\Http\ApiV1\Modules\Discounts\Requests\CalculateForCatalogRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\CalculateForCheckoutRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\CreateDiscountRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\ReplaceDiscountRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\MassStatusUpdateRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountRequest;
use App\Http\ApiV1\Modules\Discounts\Resources\CalculatorForCatalogResource;
use App\Http\ApiV1\Modules\Discounts\Resources\CalculatorForCheckoutResource;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountsResource;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountStatusesResource;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountTypesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

/**
 * Class DiscountsController
 * @package App\Http\ApiV1\Modules\Discounts\Controllers
 */
class DiscountsController
{
    public function get(int $discountId, DiscountsQuery $query): DiscountsResource
    {
        return new DiscountsResource($query->findOrFail($discountId));
    }

    public function create(CreateDiscountRequest $request, CreateDiscountAction $action): DiscountsResource
    {
        return new DiscountsResource($action->execute($request->validated()));
    }

    public function replace(int $discountId, ReplaceDiscountRequest $request, ReplaceDiscountAction $action)
    {
        return new DiscountsResource($action->execute($discountId, $request->validated()));
    }

    public function patch(int $discountId, PatchDiscountRequest $request, PatchDiscountAction $action)
    {
        return new DiscountsResource($action->execute($discountId, $request->validated()));
    }

    public function delete(int $discountId, DeleteDiscountAction $action)
    {
        $action->execute($discountId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountsQuery $query)
    {
        return DiscountsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function massStatusUpdate(MassStatusUpdateRequest $request, MassStatusUpdateAction $action)
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function calculateForCatalog(CalculateForCatalogRequest $request)
    {
        $fields = $request->validated();
        $params = (new CalculatorBuilder())
            ->offers($fields['offers'] ?? [])
            ->getParams();

        $result = (new CatalogCalculator($params))->calculate();

        return new CalculatorForCatalogResource($result);
    }

    public function calculateForCheckout(CalculateForCheckoutRequest $request)
    {
        $fields = $request->validated();
        $params = (new CalculatorBuilder())
            ->offers($fields['offers'] ?? [])
            ->customer($fields['customer'] ?? [])
            ->deliveries($fields['deliveries'] ?? [])
            ->payment($fields['payment'] ?? [])
            ->promoCode($fields['promoCode'] ?? null)
            ->getParams();

        $result = (new CheckoutCalculator($params))->calculate();

        return new CalculatorForCheckoutResource($result);
    }

    public function getDiscountStatuses()
    {
        return DiscountStatusesResource::collection(DiscountStatus::all());
    }

    public function getDiscountTypes()
    {
        return DiscountTypesResource::collection(DiscountType::all());
    }
}
