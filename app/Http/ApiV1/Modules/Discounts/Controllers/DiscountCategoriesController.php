<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\DiscountCategory\CreateDiscountCategoryAction;
use App\Domain\Discounts\Actions\DiscountCategory\DeleteDiscountCategoryAction;
use App\Domain\Discounts\Actions\DiscountCategory\PatchDiscountCategoryAction;
use App\Http\ApiV1\Modules\Discounts\Queries\DiscountCategoriesQuery;
use App\Http\ApiV1\Modules\Discounts\Requests\CreateOrReplaceDiscountCategoryRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountCategoryRequest;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountCategoriesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

/**
 * Class DiscountCategoriesController
 * @package App\Http\ApiV1\Modules\Discounts\Controllers
 */
class DiscountCategoriesController
{
    public function get(int $discountCategoryId, DiscountCategoriesQuery $query): DiscountCategoriesResource
    {
        return new DiscountCategoriesResource($query->findOrFail($discountCategoryId));
    }

    public function create(CreateOrReplaceDiscountCategoryRequest $request, CreateDiscountCategoryAction $action): DiscountCategoriesResource
    {
        return new DiscountCategoriesResource($action->execute($request->validated()));
    }

    public function patch(int $discountCategoryId, PatchDiscountCategoryRequest $request, PatchDiscountCategoryAction $action)
    {
        return new DiscountCategoriesResource($action->execute($discountCategoryId, $request->validated()));
    }

    public function delete(int $discountCategoryId, DeleteDiscountCategoryAction $action)
    {
        $action->execute($discountCategoryId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountCategoriesQuery $query)
    {
        return DiscountCategoriesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
