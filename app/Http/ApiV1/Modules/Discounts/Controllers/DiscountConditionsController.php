<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\DiscountCondition\CreateDiscountConditionAction;
use App\Domain\Discounts\Actions\DiscountCondition\DeleteDiscountConditionAction;
use App\Domain\Discounts\Actions\DiscountCondition\PatchDiscountConditionAction;
use App\Domain\Discounts\Data\DiscountConditionType;
use App\Domain\Discounts\Data\DiscountConditionTypeProp;
use App\Http\ApiV1\Modules\Discounts\Queries\DiscountConditionsQuery;
use App\Http\ApiV1\Modules\Discounts\Requests\CreateOrReplaceDiscountConditionRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountConditionRequest;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountConditionsResource;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountConditionTypePropsResource;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountConditionTypesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

/**
 * Class DiscountConditionsController
 * @package App\Http\ApiV1\Modules\Discounts\Controllers
 */
class DiscountConditionsController
{
    public function get(int $discountOfferId, DiscountConditionsQuery $query): DiscountConditionsResource
    {
        return new DiscountConditionsResource($query->findOrFail($discountOfferId));
    }

    public function create(CreateOrReplaceDiscountConditionRequest $request, CreateDiscountConditionAction $action): DiscountConditionsResource
    {
        return new DiscountConditionsResource($action->execute($request->validated()));
    }

    public function patch(int $discountOfferId, PatchDiscountConditionRequest $request, PatchDiscountConditionAction $action)
    {
        return new DiscountConditionsResource($action->execute($discountOfferId, $request->validated()));
    }

    public function delete(int $discountOfferId, DeleteDiscountConditionAction $action)
    {
        $action->execute($discountOfferId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountConditionsQuery $query)
    {
        return DiscountConditionsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function getDiscountConditionTypes()
    {
        return DiscountConditionTypesResource::collection(DiscountConditionType::all());
    }

    public function getDiscountConditionTypeProps()
    {
        return DiscountConditionTypePropsResource::collection(DiscountConditionTypeProp::all());
    }
}
