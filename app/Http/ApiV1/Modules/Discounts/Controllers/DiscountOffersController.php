<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\DiscountOffer\CreateDiscountOfferAction;
use App\Domain\Discounts\Actions\DiscountOffer\DeleteDiscountOfferAction;
use App\Domain\Discounts\Actions\DiscountOffer\PatchDiscountOfferAction;
use App\Http\ApiV1\Modules\Discounts\Queries\DiscountOffersQuery;
use App\Http\ApiV1\Modules\Discounts\Requests\CreateOrReplaceDiscountOfferRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountOfferRequest;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountOffersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

/**
 * Class DiscountOffersController
 * @package App\Http\ApiV1\Modules\Discounts\Controllers
 */
class DiscountOffersController
{
    public function get(int $discountOfferId, DiscountOffersQuery $query): DiscountOffersResource
    {
        return new DiscountOffersResource($query->findOrFail($discountOfferId));
    }

    public function create(CreateOrReplaceDiscountOfferRequest $request, CreateDiscountOfferAction $action): DiscountOffersResource
    {
        return new DiscountOffersResource($action->execute($request->validated()));
    }

    public function patch(int $discountOfferId, PatchDiscountOfferRequest $request, PatchDiscountOfferAction $action)
    {
        return new DiscountOffersResource($action->execute($discountOfferId, $request->validated()));
    }

    public function delete(int $discountOfferId, DeleteDiscountOfferAction $action)
    {
        $action->execute($discountOfferId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountOffersQuery $query)
    {
        return DiscountOffersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
