<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\DiscountSegment\CreateDiscountSegmentAction;
use App\Domain\Discounts\Actions\DiscountSegment\DeleteDiscountSegmentAction;
use App\Domain\Discounts\Actions\DiscountSegment\PatchDiscountSegmentAction;
use App\Http\ApiV1\Modules\Discounts\Queries\DiscountSegmentsQuery;
use App\Http\ApiV1\Modules\Discounts\Requests\CreateOrReplaceDiscountSegmentRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountSegmentRequest;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountSegmentsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

/**
 * Class DiscountSegmentsController
 * @package App\Http\ApiV1\Modules\Discounts\Controllers
 */
class DiscountSegmentsController
{
    public function get(int $discountOfferId, DiscountSegmentsQuery $query): DiscountSegmentsResource
    {
        return new DiscountSegmentsResource($query->findOrFail($discountOfferId));
    }

    public function create(CreateOrReplaceDiscountSegmentRequest $request, CreateDiscountSegmentAction $action): DiscountSegmentsResource
    {
        return new DiscountSegmentsResource($action->execute($request->validated()));
    }

    public function patch(int $discountOfferId, PatchDiscountSegmentRequest $request, PatchDiscountSegmentAction $action)
    {
        return new DiscountSegmentsResource($action->execute($discountOfferId, $request->validated()));
    }

    public function delete(int $discountOfferId, DeleteDiscountSegmentAction $action)
    {
        $action->execute($discountOfferId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountSegmentsQuery $query)
    {
        return DiscountSegmentsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
