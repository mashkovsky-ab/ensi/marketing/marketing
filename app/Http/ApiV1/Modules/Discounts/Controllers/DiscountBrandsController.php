<?php

namespace App\Http\ApiV1\Modules\Discounts\Controllers;

use App\Domain\Discounts\Actions\DiscountBrand\CreateDiscountBrandAction;
use App\Domain\Discounts\Actions\DiscountBrand\DeleteDiscountBrandAction;
use App\Domain\Discounts\Actions\DiscountBrand\PatchDiscountBrandAction;
use App\Http\ApiV1\Modules\Discounts\Queries\DiscountBrandsQuery;
use App\Http\ApiV1\Modules\Discounts\Requests\CreateOrReplaceDiscountBrandRequest;
use App\Http\ApiV1\Modules\Discounts\Requests\PatchDiscountBrandRequest;
use App\Http\ApiV1\Modules\Discounts\Resources\DiscountBrandsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

/**
 * Class DiscountBrandsController
 * @package App\Http\ApiV1\Modules\Discounts\Controllers
 */
class DiscountBrandsController
{
    public function get(int $discountBrandId, DiscountBrandsQuery $query): DiscountBrandsResource
    {
        return new DiscountBrandsResource($query->findOrFail($discountBrandId));
    }

    public function create(CreateOrReplaceDiscountBrandRequest $request, CreateDiscountBrandAction $action): DiscountBrandsResource
    {
        return new DiscountBrandsResource($action->execute($request->validated()));
    }

    public function patch(int $discountBrandId, PatchDiscountBrandRequest $request, PatchDiscountBrandAction $action)
    {
        return new DiscountBrandsResource($action->execute($discountBrandId, $request->validated()));
    }

    public function delete(int $discountBrandId, DeleteDiscountBrandAction $action)
    {
        $action->execute($discountBrandId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, DiscountBrandsQuery $query)
    {
        return DiscountBrandsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
