<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateOrReplaceDiscountRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class ReplaceDiscountRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'seller_id' => ['nullable', 'integer'],
            'user_id' => ['required', 'integer'],
            'name' => ['required', 'string'],
            'value_type' => ['required', 'integer', Rule::in(DiscountValueTypeEnum::cases())],
            'value' => ['required', 'integer'],
            'status' => ['required', 'integer', Rule::in(DiscountStatusEnum::cases())],
            'start_date' => ['nullable', 'string'],
            'end_date' => ['nullable', 'string'],
            'promo_code_only' => ['required', 'boolean'],

            /** OffersResource **/
            'offers' => ['array'],
            'offers.*.offer_id' => ['required_with:offers', 'integer'],
            'offers.*.except' => ['required_with:offers', 'boolean'],

            /** BrandsResource **/
            'brands' => ['array'],
            'brands.*.brand_id' => ['required_with:brands', 'integer'],
            'brands.*.except' => ['required_with:brands', 'boolean'],

            /** CategoriesResource **/
            'categories' => ['array'],
            'categories.*.category_id' => ['required_with:categories', 'integer'],

            /** SegmentsResource **/
            'segments' => ['nullable', 'array'],
            'segments.*.segment_id' => ['required_with:segments', 'integer'],

            /** ConditionsResource **/
            'conditions' => ['nullable', 'array'],
            'conditions.*.type' => [
                'required_with:conditions',
                'integer',
                Rule::in(DiscountConditionTypeEnum::cases()),
            ],
            'conditions.*.condition' => ['required_with:conditions', 'array'],
        ];
    }
}
