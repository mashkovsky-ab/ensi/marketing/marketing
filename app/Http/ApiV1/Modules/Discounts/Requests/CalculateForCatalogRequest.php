<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CalculateForCatalogRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class CalculateForCatalogRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'offers' => ['required', 'array'],
            'offers.*.offer_id' => ['required', 'integer'],
            'offers.*.product_id' => ['required', 'integer'],
            'offers.*.category_id' => ['required', 'integer'],
            'offers.*.brand_id' => ['required', 'integer', 'nullable'],
            'offers.*.price' => ['required', 'numeric'],
        ];
    }
}
