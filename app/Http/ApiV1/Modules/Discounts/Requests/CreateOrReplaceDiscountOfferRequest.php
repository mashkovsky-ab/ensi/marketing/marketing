<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateOrReplaceDiscountOfferRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class CreateOrReplaceDiscountOfferRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'discount_id' => ['required', 'integer', 'exists:discounts,id'],
            'offer_id' => ['required', 'integer'],
            'except' => ['required', 'boolean'],
        ];
    }
}
