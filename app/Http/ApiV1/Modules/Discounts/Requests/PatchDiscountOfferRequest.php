<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class PatchDiscountOfferRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class PatchDiscountOfferRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'offer_id' => ['integer'],
            'except' => ['boolean'],
        ];
    }
}
