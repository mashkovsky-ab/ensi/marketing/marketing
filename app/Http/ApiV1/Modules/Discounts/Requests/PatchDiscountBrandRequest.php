<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class PatchDiscountBrandRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class PatchDiscountBrandRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'brand_id' => ['integer'],
            'except' => ['boolean'],
        ];
    }
}
