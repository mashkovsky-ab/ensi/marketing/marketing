<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class PatchDiscountSegmentRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class PatchDiscountSegmentRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'segment_id' => ['integer'],
        ];
    }
}
