<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class MassStatusUpdateRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class MassStatusUpdateRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => ['required', 'array'],
            'id.*' => ['integer'],
            'status' => ['required', 'integer', Rule::in(DiscountStatusEnum::cases())],
        ];
    }
}
