<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CreateOrReplaceDiscountSegmentRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class CreateOrReplaceDiscountSegmentRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'discount_id' => ['required', 'integer', 'exists:discounts,id'],
            'segment_id' => ['required', 'integer'],
        ];
    }
}
