<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountValueTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PatchDiscountRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class PatchDiscountRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'seller_id' => ['nullable', 'integer'],
            'user_id' => ['integer'],
            'name' => ['string'],
            'value_type' => ['integer', Rule::in(DiscountValueTypeEnum::cases())],
            'value' => ['integer'],
            'status' => ['integer', Rule::in(DiscountStatusEnum::cases())],
            'start_date' => ['nullable', 'string'],
            'end_date' => ['nullable', 'string'],
            'promo_code_only' => ['boolean'],
        ];
    }
}
