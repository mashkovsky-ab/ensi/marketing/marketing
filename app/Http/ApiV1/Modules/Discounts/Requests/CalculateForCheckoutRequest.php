<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

/**
 * Class CalculateForCheckoutRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class CalculateForCheckoutRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            /** Offers */
            'offers' => ['required', 'array'],
            'offers.*.offer_id' => ['required', 'integer'],
            'offers.*.product_id' => ['required', 'integer'],
            'offers.*.category_id' => ['required', 'integer'],
            'offers.*.brand_id' => ['required', 'integer', 'nullable'],
            'offers.*.price' => ['required', 'numeric'],
            'offers.*.qty' => ['required', 'numeric'],

            /** Deliveries */
            'deliveries' => ['array'],
            'deliveries.*.method' => ['required_with:deliveries', 'integer'],
            'deliveries.*.price' => ['required_with:deliveries', 'numeric'],
            'deliveries.*.selected' => ['required_with:deliveries', 'boolean'],

            /** Payment */
            'payment' => ['array'],
            'payment.method' => ['required_with:payment', 'integer'],

            /** Customer */
            'customer' => ['array'],
            'customer.id' => ['required_with:customer', 'integer'],
            'customer.segment' => ['required_with:customer', 'integer'],

            /** Promo Code */
            'promoCode' => ['string', 'nullable'],
        ];
    }
}
