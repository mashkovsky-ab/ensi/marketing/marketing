<?php

namespace App\Http\ApiV1\Modules\Discounts\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypePropEnum as KeyEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PatchDiscountConditionRequest
 * @package App\Http\ApiV1\Modules\Discounts\Requests
 */
class PatchDiscountConditionRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'condition' => ['nullable', 'array'],
            'condition.*' => Rule::in(KeyEnum::cases()),

            /** Condition Min Price */
            'condition.' . KeyEnum::FIELD_MIN_PRICE => [
                'integer',
                'gt:0',
            ],

            /** Condition Brands */
            'condition.' . KeyEnum::FIELD_BRANDS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_BRANDS . '.*' => ['integer'],

            /** Condition Categories */
            'condition.' . KeyEnum::FIELD_CATEGORIES => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_CATEGORIES . '.*' => ['integer'],

            /** Condition Count */
            'condition.' . KeyEnum::FIELD_COUNT => [
                'integer',
                'gt:0',
            ],
            'condition.' . KeyEnum::FIELD_OFFER => [
                'integer',
            ],

            /** Condition Delivery Methods */
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_DELIVERY_METHODS . '.*' => ['integer'],

            /** Condition Payment Methods */
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS => [
                'array',
            ],
            'condition.' . KeyEnum::FIELD_PAYMENT_METHODS . '.*' => ['integer'],
        ];
    }
}
