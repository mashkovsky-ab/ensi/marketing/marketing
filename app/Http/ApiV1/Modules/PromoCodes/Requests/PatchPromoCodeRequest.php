<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class PatchPromoCodeRequest
 * @package App\Http\ApiV1\Modules\PromoCodes\Requests
 */
class PatchPromoCodeRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'creator_id' => ['integer'],
            'seller_id' => ['integer'],
            'owner_id' => ['nullable', 'integer'],
            'name' => ['string'],
            'code' => Rule::unique('promo_codes')->ignore((int) $this->route('promoCodeId')),
            'counter' => ['nullable', 'integer', 'gt:0'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'status' => ['integer', Rule::in(PromoCodeStatusEnum::cases())],
            'discount_id' => ['nullable', 'integer', 'exists:discounts,id'],
            'conditions' => ['nullable', 'array'],
            'conditions.segments' => ['nullable', 'array'],
            'conditions.segments.*' => ['nullable', 'integer'],
            'conditions.customers' => ['nullable', 'array'],
            'conditions.customers.*' => ['nullable', 'integer'],
        ];
    }
}
