<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CreateOrReplacePromoCodeRequest
 * @package App\Http\ApiV1\Modules\PromoCodes\Requests
 */
class CreateOrReplacePromoCodeRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'creator_id' => ['required', 'integer'],
            'seller_id' => ['nullable', 'integer'],
            'owner_id' => ['nullable', 'integer'],
            'name' => ['required', 'string'],
            'code' => ['required', Rule::unique('promo_codes')->ignore((int) $this->route('promoCodeId'))],
            'counter' => ['nullable', 'integer', 'gt:0'],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'status' => ['required', 'integer', Rule::in(PromoCodeStatusEnum::cases())],
            'type' => ['required', 'integer', Rule::in(PromoCodeTypeEnum::cases())],
            'discount_id' => [
                'nullable',
                'integer',
                'exists:discounts,id',
                'required_if:type,' . PromoCodeTypeEnum::TYPE_DISCOUNT,
                'prohibited_unless:type,' . PromoCodeTypeEnum::TYPE_DISCOUNT,
            ],
            'conditions' => ['nullable', 'array'],
            'conditions.segments' => ['nullable', 'array'],
            'conditions.segments.*' => ['nullable', 'integer'],
            'conditions.customers' => ['nullable', 'array'],
            'conditions.customers.*' => ['nullable', 'integer'],
        ];
    }
}
