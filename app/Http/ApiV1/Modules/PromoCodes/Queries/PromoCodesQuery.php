<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Queries;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\Support\Filters\FiltersAfterStartDate;
use App\Http\ApiV1\Support\Filters\FiltersLike;
use App\Http\ApiV1\Support\Filters\FiltersBeforeEndDate;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PromoCodesQuery
 * @package App\Http\ApiV1\Modules\PromoCodes\Queries
 */
class PromoCodesQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = PromoCode::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'created_at', 'start_date', 'end_date']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('code'),
            AllowedFilter::exact('creator_id'),
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('owner_id'),
            AllowedFilter::exact('discount_id'),
            AllowedFilter::exact('start_date'),
            AllowedFilter::exact('end_date'),
            AllowedFilter::exact('status'),

            AllowedFilter::custom('name', new FiltersLike),
            AllowedFilter::custom('start_date_from', new FiltersAfterStartDate),
            AllowedFilter::custom('end_date_to', new FiltersBeforeEndDate),
            AllowedFilter::scope('is_unlimited'),
            AllowedFilter::scope('created_at_from'),
            AllowedFilter::scope('created_at_to'),
        ]);

        $this->defaultSort('id');
    }
}
