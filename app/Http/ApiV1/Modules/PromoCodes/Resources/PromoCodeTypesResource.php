<?php


namespace App\Http\ApiV1\Modules\PromoCodes\Resources;

use App\Domain\PromoCodes\Data\DiscountType;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class PromoCodeTypesResource
 * @package App\Http\ApiV1\Modules\PromoCodes\Resources
 * @mixin DiscountType
 */
class PromoCodeTypesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
