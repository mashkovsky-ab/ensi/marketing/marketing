<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Resources;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class PromoCodesResource
 * @package App\Http\ApiV1\Modules\PromoCodes\Resources
 *
 * @mixin PromoCode
 */
class PromoCodesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'creator_id' => $this->creator_id,
            'seller_id' => $this->seller_id,
            'owner_id' => $this->owner_id,
            'name' => $this->name,
            'code' => $this->code,
            'counter' => $this->counter,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'status' => $this->status,
            'type' => $this->type,
            'discount_id' => $this->discount_id,
            'conditions' => $this->conditions,
            'created_at' => $this->created_at,
        ];
    }
}
