<?php


namespace App\Http\ApiV1\Modules\PromoCodes\Resources;

use App\Domain\PromoCodes\Data\DiscountStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class PromoCodeStatusesResource
 * @package App\Http\ApiV1\Modules\PromoCodes\Resources
 * @mixin DiscountStatus
 */
class PromoCodeStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
