<?php

namespace App\Http\ApiV1\Modules\PromoCodes\Controllers;

use App\Domain\PromoCodes\Actions\CreatePromoCodeAction;
use App\Domain\PromoCodes\Actions\DeletePromoCodeAction;
use App\Domain\PromoCodes\Actions\PatchPromoCodeAction;
use App\Domain\PromoCodes\Actions\ReplacePromoCodeAction;
use App\Domain\PromoCodes\Data\PromoCodeStatus;
use App\Domain\PromoCodes\Data\PromoCodeType;
use App\Http\ApiV1\Modules\PromoCodes\Queries\PromoCodesQuery;
use App\Http\ApiV1\Modules\PromoCodes\Requests\CreateOrReplacePromoCodeRequest;
use App\Http\ApiV1\Modules\PromoCodes\Requests\PatchPromoCodeRequest;
use App\Http\ApiV1\Modules\PromoCodes\Resources\PromoCodesResource;
use App\Http\ApiV1\Modules\PromoCodes\Resources\PromoCodeStatusesResource;
use App\Http\ApiV1\Modules\PromoCodes\Resources\PromoCodeTypesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

/**
 * Class PromoCodesController
 * @package App\Http\ApiV1\Modules\PromoCodes\Controllers
 */
class PromoCodesController
{
    public function get(int $promoCodeId, PromoCodesQuery $query): PromoCodesResource
    {
        return new PromoCodesResource($query->findOrFail($promoCodeId));
    }

    public function create(CreateOrReplacePromoCodeRequest $request, CreatePromoCodeAction $action): PromoCodesResource
    {
        return new PromoCodesResource($action->execute($request->validated()));
    }

    public function replace(int $promoCodeId, CreateOrReplacePromoCodeRequest $request, ReplacePromoCodeAction $action)
    {
        return new PromoCodesResource($action->execute($promoCodeId, $request->validated()));
    }

    public function patch(int $promoCodeId, PatchPromoCodeRequest $request, PatchPromoCodeAction $action)
    {
        return new PromoCodesResource($action->execute($promoCodeId, $request->validated()));
    }

    public function delete(int $promoCodeId, DeletePromoCodeAction $action)
    {
        $action->execute($promoCodeId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, PromoCodesQuery $query)
    {
        return PromoCodesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function getPromoCodeStatuses()
    {
        return PromoCodeStatusesResource::collection(PromoCodeStatus::all());
    }

    public function getPromoCodeTypes()
    {
        return PromoCodeTypesResource::collection(PromoCodeType::all());
    }
}
