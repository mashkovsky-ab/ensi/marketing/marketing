<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/promo-codes/promo-code-statuses 200', function () {
    getJson('/api/v1/promo-codes/promo-code-statuses')
        ->assertStatus(200);
});

test('GET /api/v1/promo-codes/promo-code-types 200', function () {
    getJson('/api/v1/promo-codes/promo-code-types')
        ->assertStatus(200);
});
