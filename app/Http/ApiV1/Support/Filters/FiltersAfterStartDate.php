<?php

namespace App\Http\ApiV1\Support\Filters;

use Carbon\Carbon;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FiltersAfterStartDate implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        return $query->where(function ($query) use ($value) {
            $query->where("start_date", '>=', Carbon::parse($value))
                ->orWhereNull("start_date");
        });
    }
}
