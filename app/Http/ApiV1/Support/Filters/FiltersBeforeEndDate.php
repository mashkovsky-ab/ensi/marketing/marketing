<?php

namespace App\Http\ApiV1\Support\Filters;

use Carbon\Carbon;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FiltersBeforeEndDate implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        return $query->where(function ($query) use ($value) {
            $query->where("end_date", '<=', Carbon::parse($value))
                ->orWhereNull("end_date");
        });
    }
}
