<?php

namespace App\Http\ApiV1\Support\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class FiltersLike implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->where($property, 'ilike', "%{$value}%");
    }
}
