<?php

namespace App\Domain\Discounts\Models;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypePropEnum as TypePropEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Условие возникновения скидки"
 *
 * @property int $id
 * @property int $discount_id - ид скидки
 * @property int $type - тип условия
 * @property array $condition - условие
 *
 * @property-read Discount $discount
 */
class DiscountCondition extends Model
{
    use DiscountHash;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['discount_id', 'type', 'condition'];

    const UPDATABLE = ['condition'];

    /**
     * @var array
     */
    protected $casts = [
        'condition' => 'array',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @return float|null
     */
    public function getMinPrice()
    {
        return $this->condition[TypePropEnum::FIELD_MIN_PRICE] ?? null;
    }

    /**
     * @return array
     */
    public function getBrands()
    {
        return $this->condition[TypePropEnum::FIELD_BRANDS] ?? [];
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->condition[TypePropEnum::FIELD_CATEGORIES] ?? [];
    }

    /**
     * @return int|null
     */
    public function getOffer()
    {
        return $this->condition[TypePropEnum::FIELD_OFFER] ?? null;
    }

    /**
     * @return int|null
     */
    public function getCount()
    {
        return $this->condition[TypePropEnum::FIELD_COUNT] ?? null;
    }

    /**
     * @return array
     */
    public function getDeliveryMethods()
    {
        return $this->condition[TypePropEnum::FIELD_DELIVERY_METHODS] ?? [];
    }

    /**
     * @return array
     */
    public function getPaymentMethods()
    {
        return $this->condition[TypePropEnum::FIELD_PAYMENT_METHODS] ?? [];
    }

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }

    public static function boot()
    {
        parent::boot();
    }
}
