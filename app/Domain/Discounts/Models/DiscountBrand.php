<?php

namespace App\Domain\Discounts\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Скидка на товары бренда"
 *
 * @property int $id
 * @property int $discount_id - ид скидки
 * @property int $brand_id - ид бренда
 * @property bool $except - является исключением
 *
 * @property-read Discount $discount
 */
class DiscountBrand extends Model
{
    use DiscountHash;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['discount_id', 'brand_id', 'except'];

    const UPDATABLE = ['brand_id', 'except'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }
}
