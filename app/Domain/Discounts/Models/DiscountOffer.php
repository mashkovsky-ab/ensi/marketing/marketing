<?php

namespace App\Domain\Discounts\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Скидка на оффер"
 *
 * @property int $id
 * @property int $discount_id - ид скидки
 * @property int $offer_id - ид оффера
 * @property bool $except - является исключением
 *
 * @property-read Discount $discount
 *
 */
class DiscountOffer extends Model
{
    use DiscountHash;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['discount_id', 'offer_id', 'except'];

    const UPDATABLE = ['offer_id', 'except'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }
}
