<?php

namespace App\Domain\Discounts\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Скидка сегмента пользователей"
 *
 * @property int $id
 * @property int $discount_id - ид скидки
 * @property int $segment_id - ид сегмента
 *
 * @property-read Discount $discount
 */
class DiscountSegment extends Model
{
    use DiscountHash;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['discount_id', 'segment_id'];

    const UPDATABLE = ['segment_id'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }
}
