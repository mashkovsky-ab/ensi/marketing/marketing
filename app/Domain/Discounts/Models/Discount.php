<?php

namespace App\Domain\Discounts\Models;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypeEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Класс-модель для сущности "Скидка"
 *
 * @property int $id
 * @property int $seller_id - ид продавца
 * @property int $user_id - ид покупателя
 * @property int $type - тип скидки
 * @property string $name - название скидки
 * @property int $value_type - тип значения (рубли/проценты)
 * @property int $value - размер скидки
 * @property int $status - статус
 * @property Carbon|null $start_date - дата начала действия скидки
 * @property Carbon|null $end_date - дата окончания действия скидки
 * @property bool $promo_code_only - скидка доступна только по промокоду
 * @property Carbon $created_at - дата создания скидки
 *
 * @property-read Collection|DiscountOffer[] $offers
 * @property-read Collection|DiscountBrand[] $brands
 * @property-read Collection|DiscountCategory[] $categories
 * @property-read Collection|DiscountSegment[] $segments
 * @property-read Collection|DiscountCondition[] $conditions
 *
 */
class Discount extends Model
{
    /**
     * Статус скидки
     */
    /** Создана */
    const STATUS_CREATED = 1;
    /** Отправлена на согласование */
    const STATUS_SENT = 2;
    /** На согласовании */
    const STATUS_ON_CHECKING = 3;
    /** Активна */
    const STATUS_ACTIVE = 4;
    /** Отклонена */
    const STATUS_REJECTED = 5;
    /** Приостановлена */
    const STATUS_PAUSED = 6;
    /** Завершена */
    const STATUS_EXPIRED = 7;

    /**
     * Тип скидки (назначается на)
     */
    /** Скидка на оффер */
    const DISCOUNT_TYPE_OFFER = 1;
    /** Скидка на бандл */
    const DISCOUNT_TYPE_BUNDLE = 2;
    /** Скидка на бренд */
    const DISCOUNT_TYPE_BRAND = 3;
    /** Скидка на категорию */
    const DISCOUNT_TYPE_CATEGORY = 4;
    /** Скидка на доставку */
    const DISCOUNT_TYPE_DELIVERY = 5;
    /** Скидка на сумму корзины */
    const DISCOUNT_TYPE_CART_TOTAL = 6;

    /**
     * Тип скидки для вывода в корзину/чекаут
     */
    /** Скидка "На товар" */
    const EXT_TYPE_OFFER = 1;
    /** Скидка "На доставку" */
    const EXT_TYPE_DELIVERY = 2;
    /** Скидка "На корзину" */
    const EXT_TYPE_CART = 3;
    /** Скидка "Для Вас" */
    const EXT_TYPE_PERSONAL = 4;
    /** Скидка "По промокоду" */
    const EXT_TYPE_PROMO = 5;

    /** Спонсор скидки */
    const DISCOUNT_SELLER_SPONSOR = 1;
    const DISCOUNT_ADMIN_SPONSOR = 2;

    /** Тип значения – Проценты */
    const DISCOUNT_VALUE_TYPE_PERCENT = 1;
    /** Тип значения – Рубли */
    const DISCOUNT_VALUE_TYPE_RUB = 2;

    const DISCOUNT_OFFER_RELATION = 1;
    const DISCOUNT_BRAND_RELATION = 2;
    const DISCOUNT_CATEGORY_RELATION = 3;
    const DISCOUNT_SEGMENT_RELATION = 4;
    const DISCOUNT_CONDITION_RELATION = 6;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'user_id',
        'seller_id',
        'type',
        'name',
        'value_type',
        'value',
        'status',
        'start_date',
        'end_date',
        'promo_code_only',
    ];

    const UPDATABLE = [
        'user_id',
        'seller_id',
        'name',
        'value_type',
        'value',
        'status',
        'start_date',
        'end_date',
        'promo_code_only',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var array
     */
    protected $casts = [
        'promo_code_only' => 'bool',
    ];

    /**
     * Доступные типы скидок
     * @return array
     */
    public static function availableTypes()
    {
        return [
            self::DISCOUNT_TYPE_OFFER,
            self::DISCOUNT_TYPE_BUNDLE,
            self::DISCOUNT_TYPE_BRAND,
            self::DISCOUNT_TYPE_CATEGORY,
            self::DISCOUNT_TYPE_DELIVERY,
            self::DISCOUNT_TYPE_CART_TOTAL,
        ];
    }

    /**
     * @return array
     */
    public static function availableRelations()
    {
        return [
            Discount::DISCOUNT_OFFER_RELATION,
            Discount::DISCOUNT_BRAND_RELATION,
            Discount::DISCOUNT_CATEGORY_RELATION,
            Discount::DISCOUNT_SEGMENT_RELATION,
            Discount::DISCOUNT_CONDITION_RELATION,
        ];
    }

    /**
     * @param int $discountType
     * @param array $discountConditions
     * @param bool $isPromo
     * @return int|null
     */
    public static function getExternalType(int $discountType, array $discountConditions, bool $isPromo)
    {
        if ($isPromo) {
            return self::EXT_TYPE_PROMO;
        }

        foreach ($discountConditions as $discountCondition) {
            switch ($discountCondition['type']) {
                case DiscountConditionTypeEnum::MIN_PRICE_ORDER:
                case DiscountConditionTypeEnum::MIN_PRICE_BRAND:
                case DiscountConditionTypeEnum::MIN_PRICE_CATEGORY:
                case DiscountConditionTypeEnum::EVERY_UNIT_PRODUCT:
                case DiscountConditionTypeEnum::DELIVERY_METHOD:
                case DiscountConditionTypeEnum::PAY_METHOD:
                    return self::EXT_TYPE_PERSONAL;
            }
        }

        switch ($discountType) {
            case self::DISCOUNT_TYPE_OFFER:
            case self::DISCOUNT_TYPE_BUNDLE:
            case self::DISCOUNT_TYPE_BRAND:
            case self::DISCOUNT_TYPE_CATEGORY:
                return self::EXT_TYPE_OFFER;
            case self::DISCOUNT_TYPE_DELIVERY:
                return self::EXT_TYPE_DELIVERY;
            case self::DISCOUNT_TYPE_CART_TOTAL:
                return self::EXT_TYPE_CART;
        }

        return null;
    }

    /**
     * @return array
     */
    public function getMappingRelations()
    {
        return [
            Discount::DISCOUNT_OFFER_RELATION => ['class' => DiscountOffer::class, 'items' => $this->offers],
            Discount::DISCOUNT_BRAND_RELATION => ['class' => DiscountBrand::class, 'items' => $this->brands],
            Discount::DISCOUNT_CATEGORY_RELATION => ['class' => DiscountCategory::class, 'items' => $this->categories],
            Discount::DISCOUNT_SEGMENT_RELATION => ['class' => DiscountSegment::class, 'items' => $this->segments],
            Discount::DISCOUNT_CONDITION_RELATION => ['class' => DiscountCondition::class, 'items' => $this->conditions],
        ];
    }

    /**
     * @return HasMany
     */
    public function offers()
    {
        return $this->hasMany(DiscountOffer::class, 'discount_id');
    }

    /**
     * @return HasMany
     */
    public function brands()
    {
        return $this->hasMany(DiscountBrand::class, 'discount_id');
    }

    /**
     * @return HasMany
     */
    public function categories()
    {
        return $this->hasMany(DiscountCategory::class, 'discount_id');
    }

    /**
     * @return HasMany
     */
    public function segments()
    {
        return $this->hasMany(DiscountSegment::class, 'discount_id');
    }

    /**
     * @return HasMany
     */
    public function conditions()
    {
        return $this->hasMany(DiscountCondition::class, 'discount_id');
    }

    /**
     * Активные и доступные на заданную дату скидки
     *
     * @param Builder $query
     * @param Carbon|null $date
     * @return Builder
     */
    public function scopeActive(Builder $query, ?Carbon $date = null): Builder
    {
        $date = $date ?? Carbon::now()->startOfDay();

        return $query
            ->where('status', self::STATUS_ACTIVE)
            ->where(function ($query) use ($date) {
                $query->where('start_date', '<=', $date)->orWhereNull('start_date');
            })->where(function ($query) use ($date) {
                $query->where('end_date', '>=', $date)->orWhereNull('end_date');
            });
    }

    /**
     * Скидки, которые могут быть показаны (рассчитаны) в каталоге
     * @param Builder $query
     * @return Builder
     */
    public function scopeShowInCatalog(Builder $query): Builder
    {
        return $query
            ->active()
            ->where('promo_code_only', false)
            ->whereIn('type', [
                self::DISCOUNT_TYPE_OFFER,
                self::DISCOUNT_TYPE_BRAND,
                self::DISCOUNT_TYPE_CATEGORY,
            ]);
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }

    public function scopeIsUnlimited(Builder $query, bool $value): Builder
    {
        return $query->where(function ($query) use ($value) {
            $query->whereNull('start_date', not: !$value)
                ->whereNull('end_date', not: !$value);
        });
    }

    /**
     * Проверяет корректные ли данные хранятся в сущности Discount
     * (не проверяет корректность связанных сущностей)
     * @return bool
     */
    public function validate(): bool
    {
        return $this->value >= 1 &&
            in_array($this->type, [
                self::DISCOUNT_TYPE_OFFER,
                self::DISCOUNT_TYPE_BUNDLE,
                self::DISCOUNT_TYPE_BRAND,
                self::DISCOUNT_TYPE_CATEGORY,
                self::DISCOUNT_TYPE_DELIVERY,
                self::DISCOUNT_TYPE_CART_TOTAL,
            ]) && in_array($this->value_type, [
                self::DISCOUNT_VALUE_TYPE_PERCENT,
                self::DISCOUNT_VALUE_TYPE_RUB,
            ]) && (
                $this->value_type == self::DISCOUNT_VALUE_TYPE_RUB || $this->value <= 100
            ) && in_array($this->status, [
                self::STATUS_CREATED,
                self::STATUS_SENT,
                self::STATUS_ON_CHECKING,
                self::STATUS_REJECTED,
                self::STATUS_PAUSED,
                self::STATUS_EXPIRED,
            ]) && (
                !isset($this->start_date)
                || !isset($this->end_date)
                || Carbon::parse($this->start_date)->lte(Carbon::parse($this->end_date))
            );
    }

    public static function boot()
    {
        parent::boot();

        self::saved(function (self $discount) {
            $discount->updatePimContents();
        });

        self::deleting(function (self $discount) {
            $discount->updatePimContents();
        });
    }

    /**
     * Отправление запроса на переиндексацию
     * @throws \Ensi\PimClient\ApiException
     */
    public function updatePimContents()
    {
        static $actionPerformed = false;
        if ($actionPerformed) {
            return null;
        }

        // Типы скидок, для которых нужно отправлять запрос на переиндексацию в SearchService
        $discountTypes = [
            self::DISCOUNT_TYPE_OFFER    => ['relation' => 'offers', 'column' => 'offer_id'],
            self::DISCOUNT_TYPE_BRAND    => ['relation' => 'brands', 'column' => 'brand_id'],
            self::DISCOUNT_TYPE_CATEGORY => ['relation' => 'categories', 'column' => 'category_id'],
        ];

        if (!isset($discountTypes[$this->type])) {
            return;
        }
        $column       = $discountTypes[$this->type]['column'];
        $relationName = $discountTypes[$this->type]['relation'];

        /** @var Collection $prevRelations */
        $prevRelations = $this->{$relationName};

        $this->refresh();
        /** @var Collection $newRelations */
        $newRelations = $this->{$relationName};

        $prevValues = $prevRelations->pluck($column);
        $newValues  = $newRelations->pluck($column);
        $values     = $prevValues->concat($newValues)->unique()->values()->toArray();
        if (empty($values)) {
            return;
        }

        // TODO Реализовать отправку событий в kafka для индексирования в сервисе search, когда он будет

        $actionPerformed = true;
    }
}
