<?php

namespace App\Domain\Discounts\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Скидка на товары категории"
 *
 * @property int $id
 * @property int $discount_id - ид скидки
 * @property int $category_id - ид категории
 *
 * @property-read Discount $discount
 */
class DiscountCategory extends Model
{
    use DiscountHash;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['discount_id', 'category_id'];

    const UPDATABLE = ['category_id'];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }
}
