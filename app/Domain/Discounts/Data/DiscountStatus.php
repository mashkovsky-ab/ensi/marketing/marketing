<?php


namespace App\Domain\Discounts\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountStatusEnum;

class DiscountStatus
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            DiscountStatusEnum::STATUS_CREATED => 'Создана',
            DiscountStatusEnum::STATUS_SENT => 'Отправлена на согласование',
            DiscountStatusEnum::STATUS_ON_CHECKING => 'На согласовании',
            DiscountStatusEnum::STATUS_ACTIVE => 'Активна',
            DiscountStatusEnum::STATUS_REJECTED => 'Отклонена',
            DiscountStatusEnum::STATUS_PAUSED => 'Приостановлена',
            DiscountStatusEnum::STATUS_EXPIRED => 'Завершена',
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (DiscountStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
