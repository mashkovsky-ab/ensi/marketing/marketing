<?php


namespace App\Domain\Discounts\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypePropEnum;

class DiscountConditionTypeProp
{
    public string $name;
    public string $type;

    public function __construct(public string $id)
    {
        $this->fillNameById();
        $this->fillTypeById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            DiscountConditionTypePropEnum::FIELD_MIN_PRICE => 'Минимальная стоимость',
            DiscountConditionTypePropEnum::FIELD_BRANDS => 'Идентификаторы брендов',
            DiscountConditionTypePropEnum::FIELD_CATEGORIES => 'Идентификаторы категорий',
            DiscountConditionTypePropEnum::FIELD_OFFER => 'Идентификатор оффера',
            DiscountConditionTypePropEnum::FIELD_COUNT => 'Количество товара',
            DiscountConditionTypePropEnum::FIELD_DELIVERY_METHODS => 'Методы доставки',
            DiscountConditionTypePropEnum::FIELD_PAYMENT_METHODS => 'Методы оплаты',
        };
    }

    protected function fillTypeById()
    {
        $this->type = match ($this->id) {
            DiscountConditionTypePropEnum::FIELD_MIN_PRICE => 'integer',
            DiscountConditionTypePropEnum::FIELD_BRANDS => 'array-of-integer',
            DiscountConditionTypePropEnum::FIELD_CATEGORIES => 'array-of-integer',
            DiscountConditionTypePropEnum::FIELD_OFFER => 'integer',
            DiscountConditionTypePropEnum::FIELD_COUNT => 'integer',
            DiscountConditionTypePropEnum::FIELD_DELIVERY_METHODS => 'array-of-integer',
            DiscountConditionTypePropEnum::FIELD_PAYMENT_METHODS => 'array-of-integer',
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (DiscountConditionTypePropEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
