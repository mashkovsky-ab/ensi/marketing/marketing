<?php


namespace App\Domain\Discounts\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;

class DiscountType
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            DiscountTypeEnum::DISCOUNT_TYPE_OFFER => 'Оффер',
            DiscountTypeEnum::DISCOUNT_TYPE_BRAND => 'Бренд товара',
            DiscountTypeEnum::DISCOUNT_TYPE_CATEGORY => 'Категорию товара',
            DiscountTypeEnum::DISCOUNT_TYPE_DELIVERY => 'Доставку',
            DiscountTypeEnum::DISCOUNT_TYPE_CART_TOTAL => 'Сумму корзины',
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (DiscountTypeEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
