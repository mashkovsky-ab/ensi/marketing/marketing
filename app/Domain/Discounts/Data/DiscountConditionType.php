<?php


namespace App\Domain\Discounts\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypePropEnum;

class DiscountConditionType
{
    public string $name;
    public array $props;

    public function __construct(public int $id)
    {
        $this->fillNameById();
        $this->fillPropsById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            DiscountConditionTypeEnum::MIN_PRICE_ORDER => 'Заказ от определенной суммы',
            DiscountConditionTypeEnum::MIN_PRICE_BRAND => 'Заказ от определенной суммы товаров заданного бренда',
            DiscountConditionTypeEnum::MIN_PRICE_CATEGORY => 'Заказ от определенной суммы товаров заданной категории',
            DiscountConditionTypeEnum::EVERY_UNIT_PRODUCT => 'Количество единиц одного товара',
            DiscountConditionTypeEnum::DELIVERY_METHOD => 'Способ доставки',
            DiscountConditionTypeEnum::PAY_METHOD => 'Способ оплаты',
        };
    }

    protected function fillPropsById()
    {
        $this->props = match ($this->id) {
            DiscountConditionTypeEnum::MIN_PRICE_ORDER => [
                DiscountConditionTypePropEnum::FIELD_MIN_PRICE,
            ],
            DiscountConditionTypeEnum::MIN_PRICE_BRAND => [
                DiscountConditionTypePropEnum::FIELD_MIN_PRICE,
                DiscountConditionTypePropEnum::FIELD_BRANDS,
            ],
            DiscountConditionTypeEnum::MIN_PRICE_CATEGORY => [
                DiscountConditionTypePropEnum::FIELD_MIN_PRICE,
                DiscountConditionTypePropEnum::FIELD_CATEGORIES,
            ],
            DiscountConditionTypeEnum::EVERY_UNIT_PRODUCT => [
                DiscountConditionTypePropEnum::FIELD_COUNT,
                DiscountConditionTypePropEnum::FIELD_OFFER,
            ],
            DiscountConditionTypeEnum::DELIVERY_METHOD => [
                DiscountConditionTypePropEnum::FIELD_DELIVERY_METHODS,
            ],
            DiscountConditionTypeEnum::PAY_METHOD => [
                DiscountConditionTypePropEnum::FIELD_PAYMENT_METHODS,
            ],
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (DiscountConditionTypeEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
