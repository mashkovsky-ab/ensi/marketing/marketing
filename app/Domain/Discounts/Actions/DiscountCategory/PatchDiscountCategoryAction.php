<?php

namespace App\Domain\Discounts\Actions\DiscountCategory;

use App\Domain\Discounts\Models\DiscountCategory;
use Illuminate\Support\Arr;

/**
 * Class PatchDiscountCategoryAction
 * @package App\Domain\Discounts\Actions\DiscountCategory
 */
class PatchDiscountCategoryAction
{
    public function execute(int $discountId, array $fields): DiscountCategory
    {
        $discountOffer = DiscountCategory::findOrFail($discountId);
        $discountOffer->update(Arr::only($fields, DiscountCategory::UPDATABLE));

        return $discountOffer;
    }
}
