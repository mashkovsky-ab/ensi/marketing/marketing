<?php

namespace App\Domain\Discounts\Actions\DiscountCategory;

use App\Domain\Discounts\Models\DiscountCategory;

/**
 * Class DeleteDiscountCategoryAction
 * @package App\Domain\Discounts\Actions\DiscountCategory
 */
class DeleteDiscountCategoryAction
{
    public function execute(int $discountCategoryId): void
    {
        DiscountCategory::destroy($discountCategoryId);
    }
}
