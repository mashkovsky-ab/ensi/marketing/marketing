<?php

namespace App\Domain\Discounts\Actions\DiscountCategory;

use App\Domain\Discounts\Models\DiscountCategory;
use Illuminate\Support\Arr;

/**
 * Class CreateDiscountCategoryAction
 * @package App\Domain\Discounts\Actions\DiscountCategory
 */
class CreateDiscountCategoryAction
{
    public function execute(array $fields): DiscountCategory
    {
        return DiscountCategory::create(Arr::only($fields, DiscountCategory::FILLABLE));
    }
}
