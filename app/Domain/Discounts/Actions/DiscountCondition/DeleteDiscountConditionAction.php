<?php

namespace App\Domain\Discounts\Actions\DiscountCondition;

use App\Domain\Discounts\Models\DiscountCondition;

/**
 * Class DeleteDiscountConditionAction
 * @package App\Domain\Discounts\Actions\DiscountCondition
 */
class DeleteDiscountConditionAction
{
    public function execute(int $discountConditionId): void
    {
        DiscountCondition::destroy($discountConditionId);
    }
}
