<?php

namespace App\Domain\Discounts\Actions\DiscountCondition;

use App\Domain\Discounts\Models\DiscountCondition;
use Illuminate\Support\Arr;

/**
 * Class PatchDiscountConditionAction
 * @package App\Domain\Discounts\Actions\DiscountCondition
 */
class PatchDiscountConditionAction
{
    public function execute(int $discountId, array $fields): DiscountCondition
    {
        $discountOffer = DiscountCondition::findOrFail($discountId);
        $discountOffer->update(Arr::only($fields, DiscountCondition::UPDATABLE));

        return $discountOffer;
    }
}
