<?php

namespace App\Domain\Discounts\Actions\DiscountCondition;

use App\Domain\Discounts\Models\DiscountCondition;
use Illuminate\Support\Arr;

/**
 * Class CreateDiscountConditionAction
 * @package App\Domain\Discounts\Actions\DiscountCondition
 */
class CreateDiscountConditionAction
{
    public function execute(array $fields): DiscountCondition
    {
        return DiscountCondition::create(Arr::only($fields, DiscountCondition::FILLABLE));
    }
}
