<?php

namespace App\Domain\Discounts\Actions\DiscountOffer;

use App\Domain\Discounts\Models\DiscountOffer;

/**
 * Class DeleteDiscountOfferAction
 * @package App\Domain\Discounts\Actions\DiscountOffer
 */
class DeleteDiscountOfferAction
{
    public function execute(int $discountOfferId): void
    {
        DiscountOffer::destroy($discountOfferId);
    }
}
