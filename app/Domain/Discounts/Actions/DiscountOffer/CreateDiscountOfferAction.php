<?php

namespace App\Domain\Discounts\Actions\DiscountOffer;

use App\Domain\Discounts\Models\DiscountOffer;
use Illuminate\Support\Arr;

/**
 * Class CreateDiscountOfferAction
 * @package App\Domain\Discounts\Actions\DiscountOffer
 */
class CreateDiscountOfferAction
{
    public function execute(array $fields): DiscountOffer
    {
        return DiscountOffer::create(Arr::only($fields, DiscountOffer::FILLABLE));
    }
}
