<?php

namespace App\Domain\Discounts\Actions\DiscountOffer;

use App\Domain\Discounts\Models\DiscountOffer;
use Illuminate\Support\Arr;

/**
 * Class PatchDiscountOfferAction
 * @package App\Domain\Discounts\Actions\DiscountOffer
 */
class PatchDiscountOfferAction
{
    public function execute(int $discountId, array $fields): DiscountOffer
    {
        $discountOffer = DiscountOffer::findOrFail($discountId);
        $discountOffer->update(Arr::only($fields, DiscountOffer::UPDATABLE));

        return $discountOffer;
    }
}
