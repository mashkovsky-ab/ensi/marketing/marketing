<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Models\Discount;

/**
 * Class DeleteDiscountAction
 * @package App\Domain\Discounts\Actions\Discount
 */
class DeleteDiscountAction
{
    public function execute(int $discountId): void
    {
        Discount::destroy($discountId);
    }
}
