<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Models\Discount;
use Illuminate\Support\Arr;

/**
 * Class PatchDiscountAction
 * @package App\Domain\Discounts\Actions\Discount
 */
class PatchDiscountAction
{
    public function execute(int $discountId, array $fields): Discount
    {
        $discount = Discount::findOrFail($discountId);
        $discount->update(Arr::only($fields, Discount::UPDATABLE));

        return $discount;
    }
}
