<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Validators\DiscountTypeValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class ReplaceDiscountAction
 * @package App\Domain\Discounts\Actions\Discount
 */
class ReplaceDiscountAction
{
    public function execute(int $discountId, array $fields): Discount
    {
        $discount = Discount::findOrFail($discountId);
        DiscountTypeValidator::validate($discount, $fields);

        DB::transaction(function () use (&$discount, &$fields) {
            $newAttributes = Arr::only($fields, Discount::UPDATABLE);
            foreach (Discount::UPDATABLE as $attributeKey) {
                if (!array_key_exists($attributeKey, $newAttributes)) {
                    $newAttributes[$attributeKey] = null;
                }
            }

            $discount->update($newAttributes);

            /** OffersResource **/
            $discount->offers()->delete();
            foreach ($fields['offers'] ?? [] as $offer) {
                $discount->offers()->create($offer);
            }

            /** BrandsResource **/
            $discount->brands()->delete();
            foreach ($fields['brands'] ?? [] as $brand) {
                $discount->brands()->create($brand);
            }

            /** CategoriesResource **/
            $discount->categories()->delete();
            foreach ($fields['categories'] ?? [] as $category) {
                $discount->categories()->create($category);
            }

            /** SegmentsResource **/
            $discount->segments()->delete();
            foreach ($fields['segments'] ?? [] as $segment) {
                $discount->segments()->create($segment);
            }

            /** ConditionsResource **/
            $discount->conditions()->delete();
            foreach ($fields['conditions'] ?? [] as $condition) {
                $discount->conditions()->create($condition);
            }
        });

        return $discount->load([
            'offers',
            'brands',
            'categories',
            'segments',
            'conditions',
        ]);
    }
}
