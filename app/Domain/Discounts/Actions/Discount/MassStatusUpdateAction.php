<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Models\Discount;

/**
 * Class MassStatusUpdateAction
 * @package App\Domain\Discounts\Actions\Discount
 */
class MassStatusUpdateAction
{
    public function execute(array $fields): void
    {
        $ids = $fields['id'];
        $status = $fields['status'];
        $discounts = Discount::find($ids);
        foreach ($discounts as $discount) {
            $discount->update([
                'status' => $status,
            ]);
        }
    }
}
