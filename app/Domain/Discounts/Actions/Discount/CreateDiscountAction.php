<?php

namespace App\Domain\Discounts\Actions\Discount;

use App\Domain\Discounts\Models\Discount;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class CreateDiscountAction
 * @package App\Domain\Discounts\Actions\Discount
 */
class CreateDiscountAction
{
    public function execute(array $fields): Discount
    {
        $discount = null;
        DB::transaction(function () use (&$discount, &$fields) {
            /** @var Discount $discount */
            $discount = Discount::create(Arr::only($fields, Discount::FILLABLE));

            /** OffersResource **/
            foreach ($fields['offers'] ?? [] as $offer) {
                $discount->offers()->create($offer);
            }

            /** BrandsResource **/
            foreach ($fields['brands'] ?? [] as $brand) {
                $discount->brands()->create($brand);
            }

            /** CategoriesResource **/
            foreach ($fields['categories'] ?? [] as $category) {
                $discount->categories()->create($category);
            }

            /** SegmentsResource **/
            foreach ($fields['segments'] ?? [] as $segment) {
                $discount->segments()->create($segment);
            }

            /** ConditionsResource **/
            foreach ($fields['conditions'] ?? [] as $condition) {
                $discount->conditions()->create($condition);
            }
        });

        return $discount->load([
            'offers',
            'brands',
            'categories',
            'segments',
            'conditions',
        ]);
    }
}
