<?php

namespace App\Domain\Discounts\Actions\DiscountBrand;

use App\Domain\Discounts\Models\DiscountBrand;

/**
 * Class DeleteDiscountBrandAction
 * @package App\Domain\Discounts\Actions\DiscountBrand
 */
class DeleteDiscountBrandAction
{
    public function execute(int $discountBrandId): void
    {
        DiscountBrand::destroy($discountBrandId);
    }
}
