<?php

namespace App\Domain\Discounts\Actions\DiscountBrand;

use App\Domain\Discounts\Models\DiscountBrand;
use Illuminate\Support\Arr;

/**
 * Class CreateDiscountBrandAction
 * @package App\Domain\Discounts\Actions\DiscountBrand
 */
class CreateDiscountBrandAction
{
    public function execute(array $fields): DiscountBrand
    {
        return DiscountBrand::create(Arr::only($fields, DiscountBrand::FILLABLE));
    }
}
