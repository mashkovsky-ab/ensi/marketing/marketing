<?php

namespace App\Domain\Discounts\Actions\DiscountBrand;

use App\Domain\Discounts\Models\DiscountBrand;
use Illuminate\Support\Arr;

/**
 * Class PatchDiscountBrandAction
 * @package App\Domain\Discounts\Actions\DiscountBrand
 */
class PatchDiscountBrandAction
{
    public function execute(int $discountId, array $fields): DiscountBrand
    {
        $discountOffer = DiscountBrand::findOrFail($discountId);
        $discountOffer->update(Arr::only($fields, DiscountBrand::UPDATABLE));

        return $discountOffer;
    }
}
