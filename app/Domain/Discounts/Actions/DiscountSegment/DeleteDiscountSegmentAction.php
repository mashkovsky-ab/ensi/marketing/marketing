<?php

namespace App\Domain\Discounts\Actions\DiscountSegment;

use App\Domain\Discounts\Models\DiscountSegment;

/**
 * Class DeleteDiscountSegmentAction
 * @package App\Domain\Discounts\Actions\DiscountSegment
 */
class DeleteDiscountSegmentAction
{
    public function execute(int $discountSegmentId): void
    {
        DiscountSegment::destroy($discountSegmentId);
    }
}
