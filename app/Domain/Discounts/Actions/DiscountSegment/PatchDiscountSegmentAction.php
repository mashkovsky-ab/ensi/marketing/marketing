<?php

namespace App\Domain\Discounts\Actions\DiscountSegment;

use App\Domain\Discounts\Models\DiscountSegment;
use Illuminate\Support\Arr;

/**
 * Class PatchDiscountSegmentAction
 * @package App\Domain\Discounts\Actions\DiscountSegment
 */
class PatchDiscountSegmentAction
{
    public function execute(int $discountId, array $fields): DiscountSegment
    {
        $discountOffer = DiscountSegment::findOrFail($discountId);
        $discountOffer->update(Arr::only($fields, DiscountSegment::UPDATABLE));

        return $discountOffer;
    }
}
