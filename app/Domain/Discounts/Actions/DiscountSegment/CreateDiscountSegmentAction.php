<?php

namespace App\Domain\Discounts\Actions\DiscountSegment;

use App\Domain\Discounts\Models\DiscountSegment;
use Illuminate\Support\Arr;

/**
 * Class CreateDiscountSegmentAction
 * @package App\Domain\Discounts\Actions\DiscountSegment
 */
class CreateDiscountSegmentAction
{
    public function execute(array $fields): DiscountSegment
    {
        return DiscountSegment::create(Arr::only($fields, DiscountSegment::FILLABLE));
    }
}
