<?php

namespace App\Domain\Discounts\Validators;

use App\Domain\Discounts\Models\Discount;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountTypeEnum;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class DiscountTypeValidator
 * @package App\Domain\Discounts\Validators
 */
class DiscountTypeValidator
{
    public static function validate(Discount $discount, array $fields)
    {
        $fields['type'] = $discount->type;
        Validator::validate($fields, [
            'type' => ['required', 'integer', Rule::in(DiscountTypeEnum::cases())],
            'offers' => ['required_if:type,' . DiscountTypeEnum::DISCOUNT_TYPE_OFFER],
            'brands' => ['required_if:type,' . DiscountTypeEnum::DISCOUNT_TYPE_BRAND],
            'categories' => ['required_if:type,' . DiscountTypeEnum::DISCOUNT_TYPE_CATEGORY],
        ]);
    }
}
