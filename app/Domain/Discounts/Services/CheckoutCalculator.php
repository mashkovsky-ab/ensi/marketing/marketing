<?php

namespace App\Domain\Discounts\Services;

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountBrand;
use App\Domain\Discounts\Models\DiscountCategory;
use App\Domain\Discounts\Models\DiscountCondition;
use App\Domain\Discounts\Models\DiscountOffer;
use App\Domain\Discounts\Models\DiscountSegment;
use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\DiscountConditionTypeEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\RequestBodyPagination;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Ensi\PimClient\Api\CategoriesApi as CategoryService;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\RequestBodyPagination as PimPagination;
use Ensi\PimClient\Dto\SearchCategoriesRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Класс для расчета скидок (цен) для отображения в чекауте
 * Class CheckoutCalculator
 * @package App\Core\Discount
 */
class CheckoutCalculator
{
    /** @var bool Т.к. пока скидок нет, то с помощью этого флага вырубаем всю логику скидок для производительности */
    const DISCOUNT_DISABLED = false;

    /** @var int Цена для бесплатной доставки */
    const FREE_DELIVERY_PRICE = 0;

    /** @var int Максимально возомжная скидка в процентах */
    const HIGHEST_POSSIBLE_PRICE_PERCENT = 100;

    /** @var float Погрешность при округлении */
    const EPSILON = 1e-8;

    /**
     * Входные условия, влияющие на получения скидки
     * @var array
     */
    protected $filter;

    /**
     * Скидки, которые активированы с помощью промокода
     * @var Collection
     */
    protected $appliedDiscounts;

    /**
     * Данные подгружаемые из зависимостей Discount
     * @var Collection|Collection[]
     */
    protected $relations;

    /**
     * Список активных скидок
     * @var Collection
     */
    protected $discounts;

    /**
     * Список скидок, которые можно применить (отдельно друг от друга)
     * @var Collection
     */
    protected $possibleDiscounts;

    /**
     * Список промокодов
     * @var Collection
     */
    protected $promoCodes;

    /**
     * Список примененных промокодов
     * @var Collection
     */
    protected $appliedPromoCodes;

    /**
     * Список категорий
     * @var Category[]|Collection
     */
    protected $categories;

    /**
     * Применить бесплатную доставку
     * @var bool
     */
    protected $freeDelivery = false;

    /**
     * Офферы со скидками в формате:
     * [offer_id => [['id' => discount_id, 'value' => value, 'value_type' => value_type], ...]]
     * @var Collection
     */
    protected $offersByDiscounts;

    /**
     * @var int
     */
    protected $precisionPrice;

    /**
     * @var float
     */
    protected $defaultMinPrice;

    /**
     * DiscountCalculator constructor.
     * @param array $params
     * Формат:
     *  {
     *      'customer': ['id' => int],
     *      'offers': [['id' => int, 'qty' => int|null], ...]]
     *      'promoCode': string|null
     *      'deliveries': [['method' => int, 'price' => int, 'selected' => bool], ...]
     *      'payment': ['method' => int]
     *  }
     */
    public function __construct(array $params)
    {
        $this->filter = [];
        $this->filter['bundles'] = []; // todo
        $this->filter['offers'] = $params['offers'];
        $this->filter['promoCode'] = $params['promoCode'] ?? null;
        $this->filter['customer'] = [
            'id' => isset($params['customer']['id']) ? intval($params['customer']['id']) : null,
            'segment' => isset($params['customer']['segment']) ? intval($params['customer']['segment']) : null,
        ];
        $this->filter['payment'] = [
            'method' => isset($params['payment']['method']) ? intval($params['payment']['method']) : null,
        ];

        /** Все возможные типы доставки */
        $this->filter['deliveries'] = collect();
        /** Выбранный тип доставки */
        $this->filter['selectedDelivery'] = null;
        /** Текущий тип доставки */
        $this->filter['delivery'] = null;
        if (is_iterable($params['deliveries'] ?? null)) {
            $id = 0;
            foreach ($params['deliveries'] as $delivery) {
                $id++;
                $this->filter['deliveries']->put($id, [
                    'id' => $id,
                    'price' => isset($delivery['price']) ? intval($delivery['price']) : null,
                    'method' => isset($delivery['method']) ? intval($delivery['method']) : null,
                    'selected' => isset($delivery['selected']) ? boolval($delivery['selected']) : false,
                ]);

                if ($this->filter['deliveries'][$id]['selected']) {
                    $this->filter['selectedDelivery'] = $this->filter['deliveries'][$id];
                    $this->filter['delivery'] = $this->filter['deliveries'][$id];
                }
            }
        }

        /** Доставки, для которых необходимо посчитать только возможную скидку */
        $this->filter['notSelectedDeliveries'] = $this->filter['deliveries']->filter(function ($delivery) {
            return !$delivery['selected'];
        });

        $this->promoCodes = collect();
        $this->appliedPromoCodes = collect();
        $this->discounts = collect();
        $this->possibleDiscounts = collect();
        $this->relations = collect();
        $this->categories = collect();
        $this->appliedDiscounts = collect();
        $this->offersByDiscounts = collect();

        $this->precisionPrice = config('precision.price');
        $this->defaultMinPrice = pow(10, -$this->precisionPrice);
    }

    /**
     * Возвращает данные о примененных скидках
     *
     * @return array
     */
    public function calculate(): array
    {
        if (static::DISCOUNT_DISABLED) {
            return $this->format();
        }

        $this->loadData()
            ->getActivePromoCodes()
            ->filterPromoCodes()
            ->fetchActiveDiscounts()
            ->fetchData();

        /**
         * Считаются только возможные скидки.
         * Берем все доставки, для которых необходимо посчитать только возможную скидку,
         * по очереди применяем скидки (откатывая предыдущие изменяния, т.к. нельзя выбрать сразу две доставки),
         */
        foreach ($this->filter['notSelectedDeliveries'] as $delivery) {
            $deliveryId = $delivery['id'];
            $this->filter['delivery'] = $delivery;
            $this->filter()->sort()->apply();
            $deliveryWithDiscount = $this->filter['deliveries'][$deliveryId];
            $this->rollback();
            $this->filter['deliveries'][$deliveryId] = $deliveryWithDiscount;
        }

        /** Считаются окончательные скидки */
        $this->filter['delivery'] = $this->filter['selectedDelivery'];
        $this->filter()->sort()->apply();

        return $this->format();
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return [
            'promoCode' => $this->appliedPromoCodes->values()->first(),
            'discounts' => $this->getExternalDiscountFormat(),
            'offers' => $this->getFormatOffers(),
            'deliveries' => $this->filter['deliveries']->values(),
        ];
    }

    /**
     * @return array
     */
    public function getFormatOffers(): array
    {
        return $this->filter['offers']
            ->map(function ($offer, $offerId) {
                $item = [
                    'offer_id'  => $offerId,
                    'price'     => $offer['price'],
                    'cost'      => $offer['cost'] ?? $offer['price'],
                    'change'    => isset($offer['cost']) ? $this->round($offer['cost'] - $offer['price']) : null,
                    'discounts' => $this->offersByDiscounts->has($offerId)
                        ? $this->offersByDiscounts[$offerId]->values()->toArray()
                        : [],
                ];

                if (isset($offer['qty'])) {
                    $item['qty'] = $offer['qty'];
                }

                return $item;
            })
            ->values()
            ->toArray();
    }

    /**
     * @return array
     */
    public function getExternalDiscountFormat(): array
    {
        $discounts = $this->discounts->filter(function ($discount) {
            return $this->appliedDiscounts->has($discount->id);
        })->keyBy('id');

        $items = [];
        foreach ($discounts as $discount) {
            $discountId = $discount->id;
            $conditions = $this->relations['conditions']->has($discountId)
                ? $this->relations['conditions'][$discountId]->toArray()
                : [];

            $extType = Discount::getExternalType($discount['type'], $conditions, $discount->promo_code_only);
            $items[] = [
                'id' => $discountId,
                'name' => $discount->name,
                'type' => $discount->type,
                'external_type' => $extType,
                'change' => $this->appliedDiscounts[$discountId]['change'],
                'seller_id' => $discount->seller_id,
                'visible_in_catalog' => $extType === Discount::EXT_TYPE_OFFER,
                'promo_code_only' => $discount->promo_code_only,
                'promo_code' => $discount->promo_code_only
                    ? $this->promoCodes->filter(function (PromoCode $promoCode) use ($discountId) {
                        return $promoCode->discount_id === $discountId;
                    })->first()->code
                    : null,
            ];
        }

        return $items;
    }

    /**
     * Загружает все необходимые данные
     * @return $this
     */
    protected function loadData()
    {
        $this->fetchCategories();

        $this->filter['brands'] = $this->filter['offers']->pluck('brand_id')
            ->unique()
            ->filter(function ($brandId) {
                return $brandId > 0;
            });

        $this->filter['categories'] = $this->filter['offers']->pluck('category_id')
            ->unique()
            ->filter(function ($categoryID) {
                return $categoryID > 0;
            });

        if (isset($this->filter['customer']['id'])) {
            $this->filter['customer'] = $this->getCustomerInfo((int)$this->filter['customer']['id']);
        }

        return $this;
    }

    /**
     * @param int $customerId
     * @return array
     */
    protected function getCustomerInfo(int $customerId): array
    {
        $customer = [
            'id' => $customerId,
            'segment' => 1, // todo
            'orders' => [],
        ];

        $this->filter['customer']['id'] = $customerId;

        $customer['orders']['count'] = $this->loadCustomerOrdersCount($customerId);

        return $customer;
    }

    /**
     * @param int $customerId
     * @return int
     */
    protected function loadCustomerOrdersCount(int $customerId): int
    {
        try {
            $searchOrdersRequest = new SearchOrdersRequest();
            $searchOrdersRequest->setFilter((object)[
                'customer_id' => $customerId,
            ]);
            $searchOrdersRequest->setPagination((new RequestBodyPagination())->setLimit(1));
            $response = resolve(OrdersApi::class)->searchOrders($searchOrdersRequest);
        } catch (\Exception $ex) {
            return 0;
        }

        return $response->getMeta()->getPagination()->getTotal();
    }

    /**
     * Получить все активные скидки
     *
     * @return $this
     */
    protected function fetchActiveDiscounts()
    {
        $this->discounts = Discount::query()
            ->orderBy('promo_code_only')
            ->orderBy('type')
            ->active()
            ->get([
                'id',
                'type',
                'name',
                'seller_id',
                'value',
                'value_type',
                'promo_code_only',
            ]);

        return $this;
    }

    /**
     * Получить все активные промокоды
     *
     * @return $this
     */
    protected function getActivePromoCodes()
    {
        if (!$this->filter['promoCode'] && !$this->filter['customer']['id']) {
            $this->promoCodes = collect();

            return $this;
        }

        $this->promoCodes = PromoCode::query()
            ->active()
            ->where(function (Builder $query) {
                if ($this->filter['promoCode']) {
                    $query->where('code', $this->filter['promoCode']);
                }
                if ($this->filter['customer']['id']) {
                    $query->orWhere('owner_id', $this->filter['customer']['id']);
                }
            })
            ->orderBy('owner_id')
            ->get();

        return $this;
    }

    /**
     * Загружает необходимые данные о полученных скидках ($this->discount)
     * @return $this
     */
    protected function fetchData()
    {
        $this->fetchDiscountOffers()
            ->fetchDiscountBrands()
            ->fetchDiscountCategories()
            ->fetchDiscountSegments();

        return $this;
    }

    /**
     * Фильтрует все актуальные скидки и оставляет только те, которые можно применить
     *
     * @return $this
     */
    protected function filter()
    {
        $this->possibleDiscounts = $this->discounts->filter(function (Discount $discount) {
            return $this->checkDiscount($discount);
        })->values();

        $discountIds = $this->possibleDiscounts->pluck('id');
        $this->fetchDiscountConditions($discountIds);
        $this->possibleDiscounts = $this->possibleDiscounts->filter(function (Discount $discount) {
            if ($this->relations['conditions']->has($discount->id)) {
                return $this->checkConditions($this->relations['conditions'][$discount->id]);
            }

            return true;
        })->values();

        return $this;
    }

    /**
     * @return $this
     */
    protected function filterPromoCodes()
    {
        $this->promoCodes = $this->promoCodes->filter(function (PromoCode $promoCode) {
            return $this->checkPromoCodeConditions($promoCode)
                && $this->checkPromoCodeOwner($promoCode)
                && $this->checkPromoCodeCounter($promoCode);
        });

        return $this;
    }

    /**
     * Проверяет ограничения заданные в conditions
     * @param PromoCode $promoCode
     *
     * @return bool
     */
    protected function checkPromoCodeConditions(PromoCode $promoCode): bool
    {
        if (empty($promoCode->conditions)) {
            return true;
        }

        $customerIds = $promoCode->getCustomerIds();
        if (!empty($customerIds) && !in_array($this->filter['customer']['id'], $customerIds)) {
            return false;
        }

        $segmentIds = $promoCode->getSegmentIds();
        if (!empty($segmentIds) && !in_array($this->filter['customer']['segment'], $segmentIds)) {
            return false;
        }

        return true;
    }

    /**
     * Проверяет принадлежность промокода (для РП или для Всех)
     * @param PromoCode $promoCode
     *
     * @return bool
     */
    protected function checkPromoCodeOwner(PromoCode $promoCode): bool
    {
        return !isset($promoCode->owner_id) || ($promoCode->owner_id === $this->filter['customer']['id']);
    }

    /**
     * Проверяет ограничение на количество применений одного промокода
     * @param PromoCode $promoCode
     *
     * @return bool
     */
    protected function checkPromoCodeCounter(PromoCode $promoCode): bool
    {
        if ($promoCode->counter > 0) {
            return true;
        }

        return true; // todo
    }

    /**
     * Сортирует скидки согласно приоритету
     * Скидки, имеющие наибольший приоритет помещаются в начало списка
     *
     * @return $this
     * @todo
     */
    protected function sort()
    {
        /**
         * На данный момент скидки сортируются по типу скидки
         * Если две скидки имеют одинаковый тип, то сначала берется первая по списку
         */
        return $this;
    }

    /**
     * Откатывает все примененные скидки
     * @return $this
     */
    protected function rollback()
    {
        $this->appliedDiscounts = collect();
        $this->appliedPromoCodes = collect();
        $this->offersByDiscounts = collect();

        $offers = collect();
        foreach ($this->filter['offers'] as $offer) {
            $offer['price'] = $offer['cost'] ?? $offer['price'];
            unset($offer['discount']);
            unset($offer['cost']);
            $offers->put($offer['id'], $offer);
        }
        $this->filter['offers'] = $offers;

        $deliveries = collect();
        foreach ($this->filter['deliveries'] as $delivery) {
            $delivery['price'] = $delivery['cost'] ?? $delivery['price'];
            unset($delivery['discount']);
            unset($delivery['cost']);
            $deliveries->put($delivery['id'], $delivery);
        }
        $this->filter['deliveries'] = $deliveries;

        return $this;
    }

    /**
     * Применяет промокоды и скидки
     *
     * @return $this
     */
    protected function apply()
    {
        return $this->applyPromoCodes()->applyDiscounts();
    }

    /**
     * Применяет промокоды
     * @return $this
     */
    protected function applyPromoCodes()
    {
        /** @var PromoCode $promoCode */
        foreach ($this->promoCodes as $promoCode) {
            if (!$this->isCompatiblePromoCode() || $this->appliedPromoCodes->isNotEmpty()) {
                continue;
            }

            $change = null;
            $isApply = false;
            switch ($promoCode->type) {
                case PromoCode::TYPE_DISCOUNT:
                    $discountId = $promoCode->discount_id;
                    $isPossible = $this->possibleDiscounts->pluck('id')->search($discountId) !== false;
                    $discountsById = $this->discounts->keyBy('id');
                    if ($isPossible && $discountsById->has($discountId)) {
                        $change = $this->applyDiscount($discountsById[$discountId]);
                        $isApply = $change > 0;
                    }

                    break;
                case PromoCode::TYPE_DELIVERY:
                    // Продавец не может изменять стоимость доставки
                    if ($promoCode->seller_id) {
                        break;
                    }

                    $change = 0;
                    foreach ($this->filter['deliveries'] as $k => $delivery) {
                        $changeForDelivery = $this->changePrice(
                            $delivery,
                            self::HIGHEST_POSSIBLE_PRICE_PERCENT,
                            Discount::DISCOUNT_VALUE_TYPE_PERCENT,
                            true,
                            self::FREE_DELIVERY_PRICE
                        );

                        if ($changeForDelivery > 0) {
                            $this->filter['deliveries'][$k] = $delivery;
                            $isApply = $changeForDelivery > 0;
                            $change += $changeForDelivery;
                            $this->freeDelivery = true;
                        }
                    }

                    break;
            }

            if (!$isApply) {
                continue;
            }

            $this->appliedPromoCodes->put($promoCode->code, [
                'id' => $promoCode->id,
                'type' => $promoCode->type,
                'code' => $promoCode->code,
                'isPersonal' => $promoCode->isPersonal(),
                'change' => $this->round($change),
            ]);
        }

        return $this;
    }

    /**
     * Применяет скидки
     * @return $this
     */
    protected function applyDiscounts()
    {
        /** @var Discount $discount */
        foreach ($this->possibleDiscounts as $discount) {
            // Скидки по промокодам применяются отдельно
            if ($discount->promo_code_only) {
                continue;
            }

            $this->applyDiscount($discount);
        }

        return $this;
    }

    /**
     * @param Discount $discount
     *
     * @return int|bool – На сколько изменилась цена (false – скидку невозможно применить)
     */
    protected function applyDiscount(Discount $discount)
    {
        if (!$this->isCompatibleDiscount($discount)) {
            return false;
        }

        $change = false;
        switch ($discount->type) {
            case Discount::DISCOUNT_TYPE_OFFER:
                # Скидка на офферы
                $offerIds = $this->relations['offers'][$discount->id]->pluck('offer_id');
                $change = $this->applyDiscountToOffer($discount, $offerIds);

                break;
            case Discount::DISCOUNT_TYPE_BUNDLE:
                // todo
                break;
            case Discount::DISCOUNT_TYPE_BRAND:
                # Скидка на бренды
                /** @var Collection $brandIds */
                $brandIds = $this->relations['brands'][$discount->id]->pluck('brand_id');
                # За исключением офферов
                $exceptOfferIds = $this->getExceptOffersForDiscount($discount->id);
                # Отбираем нужные офферы
                $offerIds = $this->filterForDiscountBrand($brandIds, $exceptOfferIds);
                $change = $this->applyDiscountToOffer($discount, $offerIds);

                break;
            case Discount::DISCOUNT_TYPE_CATEGORY:
                # Скидка на категории
                /** @var Collection $categoryIds */
                $categoryIds = $this->relations['categories'][$discount->id]->pluck('category_id');
                # За исключением брендов
                $exceptBrandIds = $this->getExceptBrandsForDiscount($discount->id);
                # За исключением офферов
                $exceptOfferIds = $this->getExceptOffersForDiscount($discount->id);
                # Отбираем нужные офферы
                $offerIds = $this->filterForDiscountCategory($categoryIds, $exceptBrandIds, $exceptOfferIds);
                $change = $this->applyDiscountToOffer($discount, $offerIds);

                break;
            case Discount::DISCOUNT_TYPE_DELIVERY:
                // Если используется бесплатная дотсавка (например, по промокоду), то не использовать скидку
                if ($this->freeDelivery) {
                    break;
                }

                $deliveryId = $this->filter['delivery']['id'] ?? null;
                if ($this->filter['deliveries']->has($deliveryId)) {
                    $change = $this->changePrice(
                        $this->filter['delivery'],
                        $discount->value,
                        $discount->value_type,
                        true,
                        self::FREE_DELIVERY_PRICE
                    );

                    $this->filter['deliveries'][$deliveryId] = $this->filter['delivery'];
                }

                break;
            case Discount::DISCOUNT_TYPE_CART_TOTAL:
                $change = $this->applyDiscountToBasket($discount);

                break;
        }

        if ($change > 0) {
            $this->appliedDiscounts->put($discount->id, [
                'discountId' => $discount->id,
                'change' => $this->round($change),
                'conditions' => $this->relations['conditions']->has($discount->id)
                    ? $this->relations['conditions'][$discount->id]->pluck('type')
                    : [],
            ]);
        }

        return $change;
    }

    /**
     * Применяет скидку ко всей корзине (распределяется равномерно по всем товарам)
     *
     * @param $discount
     * @return int Абсолютный размер скидки (в руб.), который удалось использовать
     */
    protected function applyDiscountToBasket($discount)
    {
        if ($this->filter['offers']->isEmpty()) {
            return false;
        }

        return $this->applyEvenly($discount, $this->filter['offers']->pluck('offer_id'));
    }

    /**
     * Равномерно распределяет скидку
     * @param $discount
     * @param Collection $offerIds
     * @return int Абсолютный размер скидки (в руб.), который удалось использовать
     */
    protected function applyEvenly($discount, Collection $offerIds)
    {
        $priceOrders = $this->getPriceOrders();
        if ($priceOrders <= 0) {
            return 0.;
        }

        # Текущее значение скидки (в рублях, без учета скидок, которые могли применяться ранее)
        $currentDiscountValue = 0;
        # Номинальное значение скидки (в рублях)
        $discountValue = $discount->value_type === Discount::DISCOUNT_VALUE_TYPE_PERCENT
            ? $this->round($priceOrders * $discount->value / 100)
            : $discount->value;
        # Скидка не может быть больше, чем стоимость всей корзины
        $discountValue = min($discountValue, $priceOrders);

        /**
         * Если скидку невозможно распределить равномерно по всем товарам,
         * то использовать скидку, сверх номинальной
         */
        $force = false;
        $offersWithDiscount = [];
        $prevCurrentDiscountValue = 0;
        while ($currentDiscountValue < $discountValue && $priceOrders !== 0) {
            /**
             * Сортирует ID офферов.
             * Сначала применяем скидки на самые дорогие товары (цена * количество)
             * Если необходимо использовать скидку сверх номинальной ($force), то сортируем в обратном порядке.
             */
            $offerIds = $this->sortOrderIdsByTotalPrice($offerIds, $force);
            $coefficient = ($discountValue - $currentDiscountValue) / $priceOrders;
            foreach ($offerIds as $offerId) {
                $offer = $this->filter['offers'][$offerId];
                $value = $this->round($offer['price'] * $coefficient);
                $totalValue = $this->round($value * $offer['qty']);
                $discountDiff = $this->round($discountValue - $currentDiscountValue);
                if ($totalValue <= $discountDiff || $force) {
                    $change = $this->changePrice($offer, $value);
                    $this->filter['offers'][$offerId] = $offer;
                } else {
                    continue;
                }

                if ($change > 0) {
                    $offersWithDiscount[$offerId] = $offersWithDiscount[$offerId] ?? 0;
                    $offersWithDiscount[$offerId] += $change;
                }

                $currentDiscountValue += $this->round($change * $offer['qty']);
                if ($currentDiscountValue >= $discountValue) {
                    break(2);
                }
            }

            $priceOrders = $this->getPriceOrders();
            if ($prevCurrentDiscountValue === $currentDiscountValue) {
                if ($force) {
                    break;
                }

                $force = true;
            }

            $prevCurrentDiscountValue = $currentDiscountValue;
        }

        foreach ($offersWithDiscount as $offerId => $change) {
            $this->addDiscountToOffer($offerId, $discount, $change);
        }

        return $currentDiscountValue;
    }

    /**
     * @param Collection $offerIds
     * @param bool $asc
     * @return Collection
     */
    protected function sortOrderIdsByTotalPrice(Collection $offerIds, $asc = true): Collection
    {
        return $offerIds->sort(function ($offerIdLft, $offerIdRgt) use ($asc) {
            $offerLft = $this->filter['offers'][$offerIdLft];
            $totalPriceLft = $this->round($offerLft['price'] * $offerLft['qty']);
            $offerRgt = $this->filter['offers'][$offerIdRgt];
            $totalPriceRgt = $this->round($offerRgt['price'] * $offerRgt['qty']);

            return ($asc ? 1 : -1) * ($totalPriceLft - $totalPriceRgt);
        });
    }

    /**
     * Вместо равномерного распределения скидки по офферам (applyEvenly), применяет скидку к каждому офферу
     *
     * @param $discount
     * @param Collection $offerIds
     * @return bool Результат применения скидки
     */
    protected function applyDiscountToOffer($discount, Collection $offerIds)
    {
        $offerIds = $offerIds->filter(function ($offerId) use ($discount) {
            return $this->applicableToOffer($offerId);
        });

        if ($offerIds->isEmpty()) {
            return false;
        }

        $changed = 0;
        foreach ($offerIds as $offerId) {
            $offer = $this->filter['offers'][$offerId];
            $change = $this->changePrice($offer, $discount->value, $discount->value_type);
            $this->filter['offers'][$offerId] = $offer;
            if ($change <= 0) {
                continue;
            }

            $this->addDiscountToOffer($offerId, $discount, $change);
            $changed += $this->round($change * $offer['qty']);
        }

        return $changed;
    }

    /**
     * @param int      $offerId
     * @param Discount $discount
     * @param float    $change
     */
    protected function addDiscountToOffer(int $offerId, Discount $discount, float $change)
    {
        if (!isset($this->offersByDiscounts[$offerId])) {
            $this->offersByDiscounts->put($offerId, collect());
        }

        $this->offersByDiscounts[$offerId]->push([
            'id' => $discount->id,
            'change' => $this->round($change),
            'value' => $discount->value,
            'value_type' => $discount->value_type,
        ]);
    }

    /**
     * Возвращает размер скидки (без учета предыдущих скидок)
     *
     * @param            $item
     * @param float      $value
     * @param int        $valueType
     * @param bool       $apply               нужно ли применять скидку
     * @param float|null $lowestPossiblePrice Самая низкая возможная цена (null - по умолчанию)
     *
     * @return int
     */
    protected function changePrice(
        &$item,
        float $value,
        int $valueType = Discount::DISCOUNT_VALUE_TYPE_RUB,
        bool $apply = true,
        ?float $lowestPossiblePrice = null
    ) {
        if (!isset($item['price']) || $value <= 0) {
            return 0;
        }

        $lowestPossiblePrice = $lowestPossiblePrice ?? $this->defaultMinPrice;
        if ($item['price'] < $lowestPossiblePrice) {
            return 0;
        }

        $currentDiscount = $item['discount'] ?? 0;
        $currentCost     = $item['cost'] ?? $item['price'];
        switch ($valueType) {
            case Discount::DISCOUNT_VALUE_TYPE_PERCENT:
                $price = $this->round($currentCost * $value / 100);
                $discountValue = min($item['price'], $price);

                break;
            case Discount::DISCOUNT_VALUE_TYPE_RUB:
                $discountValue = $value > $item['price'] ? $item['price'] : $value;

                break;
            default:
                return 0;
        }

        /** Цена не может быть меньше $lowestPossiblePrice */
        if ($item['price'] - $discountValue < $lowestPossiblePrice) {
            $discountValue = $this->round($item['price'] - $lowestPossiblePrice);
        }

        if ($apply) {
            $item['discount'] = $this->round($currentDiscount + $discountValue);
            $item['price']    = $this->round($currentCost - $item['discount']);
            $item['cost']     = $this->round($currentCost);
        }

        return $this->round($discountValue);
    }

    /**
     * @param $value
     *
     * @return float
     */
    protected function round($value): float
    {
        $precision = pow(10, $this->precisionPrice);
        $r = $value * $precision;
        $rounded = ($r - floor($r) < self::EPSILON) ? floor($r) : ceil($r);

        return round($rounded / $precision, $this->precisionPrice);
    }

    /**
     * @param $brandIds
     * @param $exceptOfferIds
     * @return Collection
     */
    protected function filterForDiscountBrand($brandIds, $exceptOfferIds): Collection
    {
        return $this->filter['offers']->filter(function ($offer) use ($brandIds, $exceptOfferIds) {
            return $brandIds->search($offer['brand_id']) !== false
                && $exceptOfferIds->search($offer['offer_id']) === false;
        })->pluck('id');
    }

    /**
     * @param $categoryIds
     * @param $exceptBrandIds
     * @param $exceptOfferIds
     * @return Collection
     */
    protected function filterForDiscountCategory($categoryIds, $exceptBrandIds, $exceptOfferIds): Collection
    {
        return $this->filter['offers']->filter(function ($offer) use ($categoryIds, $exceptBrandIds, $exceptOfferIds) {
            $offerCategory = $this->categories->has($offer['category_id'])
                ? $this->categories[$offer['category_id']]
                : null;

            return $offerCategory
                && $categoryIds->reduce(function ($carry, $categoryId) use ($offerCategory) {
                    return $carry ||
                        (
                            $this->categories->has($categoryId)
                            && $this->categories[$categoryId]->isAncestorOf($offerCategory)
                        );
                })
                && $exceptBrandIds->search($offer['brand_id']) === false
                && $exceptOfferIds->search($offer['offer_id']) === false;
        })->pluck('id');
    }

    /**
     * Совместимы ли скидки (даже если они не пересекаются)
     * @param Discount $discount
     * @return bool
     */
    protected function isCompatibleDiscount(Discount $discount): bool
    {
        return !$this->appliedDiscounts->has($discount->id);
    }

    /**
     * Совместимы ли промокоды
     *
     * @return bool
     */
    protected function isCompatiblePromoCode(): bool
    {
        return $this->appliedPromoCodes->isEmpty();
    }

    /**
     * Можно ли применить скидку к офферу
     * @param $offerId
     * @return bool
     */
    protected function applicableToOffer($offerId): bool
    {
        return $this->appliedDiscounts->isEmpty() || !$this->offersByDiscounts->has($offerId);
    }

    /**
     * Получаем все возможные скидки и офферы из DiscountOffer
     * @return $this
     */
    protected function fetchDiscountOffers()
    {
        $validTypes = [Discount::DISCOUNT_TYPE_OFFER, Discount::DISCOUNT_TYPE_BRAND, Discount::DISCOUNT_TYPE_CATEGORY];
        if ($this->filter['offers']->isEmpty() || $this->existsAnyTypeInDiscounts($validTypes)) {
            $this->relations['offers'] = collect();

            return $this;
        }

        $this->relations['offers'] = DiscountOffer::query()
            ->whereIn('discount_id', $this->discounts->pluck('id'))
            ->whereIn('offer_id', $this->filter['offers']->pluck('offer_id'))
            ->get(['discount_id', 'offer_id', 'except'])
            ->groupBy('discount_id');

        return $this;
    }

    /**
     * Получаем все возможные скидки и бренды из DiscountBrand
     * @return $this
     */
    protected function fetchDiscountBrands()
    {
        /** Если не передали офферы, то пропускаем скидки на бренды */
        $validTypes = [Discount::DISCOUNT_TYPE_BRAND, Discount::DISCOUNT_TYPE_CATEGORY];
        if ($this->filter['brands']->isEmpty() || $this->existsAnyTypeInDiscounts($validTypes)) {
            $this->relations['brands'] = collect();

            return $this;
        }

        $this->relations['brands'] = DiscountBrand::query()
            ->whereIn('discount_id', $this->discounts->pluck('id'))
            ->whereIn('brand_id', $this->filter['brands'])
            ->get(['discount_id', 'brand_id', 'except'])
            ->groupBy('discount_id');

        return $this;
    }

    /**
     * Получаем все возможные скидки и категории из DiscountCategory
     * @return $this
     */
    protected function fetchDiscountCategories()
    {
        /** Если не передали офферы, то пропускаем скидки на категорию */
        $validTypes = [Discount::DISCOUNT_TYPE_CATEGORY];
        if ($this->filter['categories']->isEmpty() || $this->existsAnyTypeInDiscounts($validTypes)) {
            $this->relations['categories'] = collect();

            return $this;
        }

        $this->relations['categories'] = DiscountCategory::query()
            ->whereIn('discount_id', $this->discounts->pluck('id'))
            ->get(['discount_id', 'category_id'])
            ->filter(function ($discountCategory) {
                if (!isset($this->categories[$discountCategory->category_id])) {
                    return false;
                }

                $categoryLeaf = $this->categories[$discountCategory->category_id];
                foreach ($this->filter['categories'] as $categoryId) {
                    if ($categoryLeaf->isSelfOrAncestorOf($this->categories[$categoryId])) {
                        return true;
                    }
                }

                return false;
            })
            ->groupBy('discount_id');

        return $this;
    }

    /**
     * Получаем все возможные скидки и сегменты из DiscountSegment
     * @return $this
     */
    protected function fetchDiscountSegments()
    {
        $this->relations['segments'] = DiscountSegment::query()
            ->whereIn('discount_id', $this->discounts->pluck('id'))
            ->get(['discount_id', 'segment_id'])
            ->groupBy('discount_id')
            ->transform(function ($segments) {
                return $segments->pluck('segment_id');
            });

        return $this;
    }

    /**
     * Получаем все возможные скидки и условия из DiscountCondition
     * @param Collection $discountIds
     * @return $this
     */
    protected function fetchDiscountConditions(Collection $discountIds)
    {
        $this->relations['conditions'] = DiscountCondition::query()
            ->whereIn('discount_id', $discountIds)
            ->get(['discount_id', 'type', 'condition'])
            ->groupBy('discount_id');

        return $this;
    }

    /**
     * Получает все категории
     * @return $this
     */
    protected function fetchCategories()
    {
        if ($this->categories->isNotEmpty()) {
            return $this;
        }

        $pagination = new PimPagination();
        $pagination->setType(PaginationTypeEnum::OFFSET);
        $pagination->setLimit(-1);
        $request = new SearchCategoriesRequest();
        $request->setPagination($pagination);

        /** @var CategoryService $categoryService */
        $categoryService = resolve(CategoryService::class);
        $this->categories = collect($categoryService->searchCategories($request)->getData())
            ->keyBy('id');

        return $this;
    }

    /**
     * @param $discountId
     * @return Collection
     */
    protected function getExceptOffersForDiscount($discountId): Collection
    {
        return $this->relations['offers']->has($discountId)
            ? $this->relations['offers'][$discountId]->filter(function ($offer) {
                return $offer['except'];
            })->pluck('offer_id')
            : collect();
    }

    /**
     * @param $discountId
     * @return Collection
     */
    protected function getExceptBrandsForDiscount($discountId): Collection
    {
        return $this->relations['brands']->has($discountId)
            ? $this->relations['brands'][$discountId]->filter(function ($brand) {
                return $brand['except'];
            })->pluck('brand_id')
            : collect();
    }

    /**
     * Существует ли хотя бы одна скидка с одним из типов скидки ($types)
     * @param array $types
     * @return bool
     */
    protected function existsAnyTypeInDiscounts(array $types): bool
    {
        return $this->discounts->groupBy('type')
            ->keys()
            ->intersect($types)
            ->isEmpty();
    }

    /**
     * Можно ли применить данную скидку (независимо от других скидок)
     *
     * @param Discount $discount
     * @return bool
     */
    protected function checkDiscount(Discount $discount): bool
    {
        return $this->checkPromo($discount)
            && $this->checkType($discount)
            && $this->checkSegment($discount);
    }

    /**
     * Если скидку можно получить только по промокоду, проверяем введен ли нужный промокод
     *
     * @param Discount $discount
     * @return bool
     */
    protected function checkPromo(Discount $discount): bool
    {
        if (!$discount->promo_code_only) {
            return true;
        }

        return $this->promoCodes->filter(function (PromoCode $promoCode) use ($discount) {
            return $promoCode->discount_id === $discount->id;
        })->isNotEmpty();
    }

    /**
     * Проверяет все необходимые условия по свойству "Тип скидки"
     *
     * @param Discount $discount
     * @return bool
     */
    protected function checkType(Discount $discount): bool
    {
        switch ($discount->type) {
            case Discount::DISCOUNT_TYPE_OFFER:
                return $this->checkOffers($discount);
            case Discount::DISCOUNT_TYPE_BUNDLE:
                return $this->checkBundles($discount);
            case Discount::DISCOUNT_TYPE_BRAND:
                return $this->checkBrands($discount);
            case Discount::DISCOUNT_TYPE_CATEGORY:
                return $this->checkCategories($discount);
            case Discount::DISCOUNT_TYPE_DELIVERY:
                return isset($this->filter['delivery']['price']);
            case Discount::DISCOUNT_TYPE_CART_TOTAL:
                return $this->filter['offers']->isNotEmpty();
            default:
                return false;
        }
    }

    /**
     * Проверяет доступность применения скидки на офферы
     *
     * @param Discount $discount
     * @return bool
     */
    protected function checkOffers(Discount $discount): bool
    {
        return $discount->type === Discount::DISCOUNT_TYPE_OFFER
            && $this->relations['offers']->has($discount->id)
            && $this->relations['offers'][$discount->id]->filter(function ($offers) {
                return !$offers['except'];
            })->isNotEmpty();
    }

    /**
     * Проверяет доступность применения скидки на бандлы
     *
     * @param Discount $discount
     * @return bool
     * @todo
     */
    protected function checkBundles(Discount $discount): bool
    {
        return $discount->type === Discount::DISCOUNT_TYPE_BUNDLE && !empty($this->filter['bundles']);
    }

    /**
     * Проверяет доступность применения скидки на бренды
     *
     * @param Discount $discount
     * @return bool
     */
    protected function checkBrands(Discount $discount): bool
    {
        return $discount->type === Discount::DISCOUNT_TYPE_BRAND
            && $this->relations['brands']->has($discount->id)
            && $this->relations['brands'][$discount->id]->filter(function ($brand) {
                return !$brand['except'];
            })->isNotEmpty();
    }

    /**
     * Проверяет доступность применения скидки на категории
     *
     * @param Discount $discount
     * @return bool
     */
    protected function checkCategories(Discount $discount): bool
    {
        return $discount->type === Discount::DISCOUNT_TYPE_CATEGORY
            && $this->relations['categories']->has($discount->id)
            && $this->relations['categories'][$discount->id]->isNotEmpty();
    }

    /**
     * @param Discount $discount
     * @return bool
     */
    protected function checkSegment(Discount $discount): bool
    {
        // Если отсутствуют условия скидки на сегмент
        if (!$this->relations['segments']->has($discount->id)) {
            return true;
        }

        return isset($this->filter['customer']['segment'])
            && $this->relations['segments'][$discount->id]->search($this->filter['customer']['segment']) !== false;
    }

    /**
     * Проверяет доступность применения скидки на все соответствующие условия
     *
     * @param Collection $conditions
     * @return bool
     * @todo
     */
    protected function checkConditions(Collection $conditions): bool
    {
        /** @var DiscountCondition $condition */
        foreach ($conditions as $condition) {
            switch ($condition->type) {
                /** Скидка на заказ от заданной суммы */
                case DiscountConditionTypeEnum::MIN_PRICE_ORDER:
                    $r = $this->getCostOrders() >= $condition->getMinPrice();

                    break;
                /** Скидка на заказ от заданной суммы на один из брендов */
                case DiscountConditionTypeEnum::MIN_PRICE_BRAND:
                    $r = $this->getMaxTotalPriceForBrands($condition->getBrands()) >= $condition->getMinPrice();

                    break;
                /** Скидка на заказ от заданной суммы на одну из категорий */
                case DiscountConditionTypeEnum::MIN_PRICE_CATEGORY:
                    $r = $this->getMaxTotalPriceForCategories($condition->getCategories()) >= $condition->getMinPrice();

                    break;
                /** Скидка на заказ определенного количества товара */
                case DiscountConditionTypeEnum::EVERY_UNIT_PRODUCT:
                    $r = $this->checkEveryUnitProduct($condition->getOffer(), $condition->getCount());

                    break;
                /** Скидка на один из методов доставки */
                case DiscountConditionTypeEnum::DELIVERY_METHOD:
                    $r = $this->checkDeliveryMethod($condition->getDeliveryMethods());

                    break;
                /** Скидка на один из методов оплаты */
                case DiscountConditionTypeEnum::PAY_METHOD:
                    $r = $this->checkPayMethod($condition->getPaymentMethods());

                    break;
                default:
                    return false;
            }

            if (!$r) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return int|null
     */
    public function getCustomerId(): ?int
    {
        return $this->filter['customer']['id'] ?? null;
    }

    /**
     * @return int|null
     */
    public function getCountOrders(): ?int
    {
        return $this->filter['customer']['orders']['count'] ?? null;
    }

    /**
     * Заказ от определенной суммы
     * @return float
     */
    public function getPriceOrders(): float
    {
        return $this->filter['offers']->map(function ($offer) {
            return $this->round($offer['price'] * $offer['qty']);
        })->sum();
    }

    /**
     * Сумма заказа без учета скидки
     * @return float
     */
    public function getCostOrders(): float
    {
        return $this->filter['offers']->map(function ($offer) {
            return $this->round(($offer['cost'] ?? $offer['price']) * $offer['qty']);
        })->sum();
    }

    /**
     * Возвращает максимальную сумму товаров среди брендов ($brands)
     * @param array $brands
     * @return float
     */
    public function getMaxTotalPriceForBrands($brands): float
    {
        $max = 0.;
        foreach ($brands as $brandId) {
            $sum = $this->filter['offers']->filter(function ($offer) use ($brandId) {
                return $offer['brand_id'] === $brandId;
            })->map(function ($offer) {
                return $this->round($offer['price'] * $offer['qty']);
            })->sum();
            $max = max($sum, $max);
        }

        return $max;
    }

    /**
     * @param array $categories
     * @return float
     */
    public function getMaxTotalPriceForCategories($categories): float
    {
        $max = 0.;
        foreach ($categories as $categoryId) {
            $sum = $this->filter['offers']->filter(function ($offer) use ($categoryId) {
                return $this->categories->has($categoryId)
                    && $this->categories->has($offer['category_id'])
                    && $this->categories[$categoryId]->isSelfOrAncestorOf($this->categories[$offer['category_id']]);
            })->map(function ($offer) {
                return $this->round($offer['price'] * $offer['qty']);
            })->sum();
            $max = max($sum, $max);
        }

        return $max;
    }

    /**
     * Количество единиц одного оффера
     * @param $offerId
     * @param $count
     * @return bool
     */
    public function checkEveryUnitProduct($offerId, $count): bool
    {
        return $this->filter['offers']->has($offerId) && $this->filter['offers'][$offerId]['qty'] >= $count;
    }

    /**
     * Способ доставки
     * @param $deliveryMethods
     * @return bool
     */
    public function checkDeliveryMethod($deliveryMethods): bool
    {
        return isset($this->filter['delivery']['method']) && in_array($this->filter['delivery']['method'], $deliveryMethods);
    }

    /**
     * Способ оплаты
     * @param $payments
     * @return bool
     */
    public function checkPayMethod($payments): bool
    {
        return isset($this->filter['payment']['method']) && in_array($this->filter['payment']['method'], $payments);
    }
}
