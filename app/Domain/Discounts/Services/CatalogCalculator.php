<?php

namespace App\Domain\Discounts\Services;

use App\Domain\Discounts\Models\Discount;
use App\Domain\Discounts\Models\DiscountBrand;
use App\Domain\Discounts\Models\DiscountCondition;
use App\Domain\Discounts\Models\DiscountOffer;
use Ensi\PimClient\ApiException;

use Illuminate\Support\Collection;

/**
 * Класс для расчета скидок (цен) для отображения в каталоге
 * Class CatalogCalculator
 * @package App\Services\Discount
 */
class CatalogCalculator extends CheckoutCalculator
{
    /**
     * @return array
     */
    public function calculate(): array
    {
        if (static::DISCOUNT_DISABLED) {
            return $this->format();
        }

        $this->loadData()
            ->fetchActiveDiscounts()
            ->fetchData()
            ->filter()
            ->sort()
            ->apply();

        return $this->format();
    }

    /**
     * @return array
     */
    public function format(): array
    {
        return $this->getFormatOffers();
    }

    /**
     * Загружает все необходимые данные
     * @return $this|CheckoutCalculator
     * @throws ApiException
     */
    protected function loadData()
    {
        $this->fetchCategories();

        $this->filter['brands'] = $this->filter['offers']->pluck('brand_id', 'brand_id')
            ->unique()
            ->filter(function ($brandId) {
                return $brandId > 0;
            });

        $this->filter['categories'] = $this->filter['offers']->pluck('category_id', 'category_id')
            ->unique()
            ->filter(function ($brandId) {
                return $brandId > 0;
            });

        return $this;
    }

    /**
     * Получаем все возможные скидки и офферы из DiscountOffer
     * @return $this
     */
    protected function fetchDiscountOffers()
    {
        $this->relations['offers'] = DiscountOffer::query()
            ->whereIn('discount_id', $this->discounts->pluck('id'))
            ->get(['discount_id', 'offer_id', 'except'])
            ->filter(function ($discountOffer) {
                return $this->filter['offers']->has($discountOffer['offer_id']);
            })
            ->groupBy('discount_id');

        return $this;
    }

    /**
     * Получаем все возможные скидки и бренды из DiscountBrand
     * @return $this
     */
    protected function fetchDiscountBrands()
    {
        $this->relations['brands'] = DiscountBrand::query()
            ->whereIn('discount_id', $this->discounts->pluck('id'))
            ->get(['discount_id', 'brand_id', 'except'])
            ->filter(function ($discountBrand) {
                return $this->filter['brands']->has($discountBrand['brand_id']);
            })
            ->groupBy('discount_id');

        return $this;
    }

    /**
     * Получаем все возможные скидки и условия из DiscountCondition
     * @param Collection $discountIds
     * @return $this
     */
    protected function fetchDiscountConditions(Collection $discountIds)
    {
        $this->relations['conditions'] = DiscountCondition::query()
            ->whereIn('discount_id', $discountIds)
            ->get(['discount_id', 'type', 'condition'])
            ->groupBy('discount_id');

        return $this;
    }

    /**
     * Проверяет доступность применения скидки на все соответствующие условия
     *
     * @param Collection $conditions
     * @return bool
     * @todo
     */
    protected function checkConditions(Collection $conditions): bool
    {
        return $conditions->isNotEmpty();
    }

    /**
     * Получить все активные скидки, которые могут быть показаны (рассчитаны) в каталоге
     *
     * @return $this
     */
    protected function fetchActiveDiscounts()
    {
        $this->discounts = Discount::query()
            ->showInCatalog()
            ->orderBy('type')
            ->get(['id', 'type', 'value', 'value_type', 'promo_code_only']);

        return $this;
    }

    /**
     * Можно ли применить данную скидку (независимо от других скидок)
     *
     * @param Discount $discount
     * @return bool
     */
    protected function checkDiscount(Discount $discount): bool
    {
        return $this->checkType($discount)
            && $this->checkSegment($discount);
    }
}
