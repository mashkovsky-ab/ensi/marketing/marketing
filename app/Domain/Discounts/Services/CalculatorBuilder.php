<?php

namespace App\Domain\Discounts\Services;

use Illuminate\Support\Collection;

/**
 * Class CalculatorBuilder
 * @package App\Core\Discount
 */
class CalculatorBuilder
{
    private $params;

    public function __construct()
    {
        $this->params = [];
        $this->params['customer'] = collect();
        $this->params['offers'] =  collect();
        $this->params['deliveries'] = collect();
        $this->params['payment'] = collect();
        $this->params['promoCode'] = null;
    }

    /**
     * @param Collection|array $customers
     *
     * @return CalculatorBuilder
     */
    public function customer($customers): CalculatorBuilder
    {
        $this->params['customer'] = collect($customers);

        return $this;
    }

    /**
     * @param Collection|array $offers
     *
     * @return CalculatorBuilder
     */
    public function offers($offers): CalculatorBuilder
    {
        $this->params['offers'] = collect();
        foreach ($offers as $offer) {
            $this->params['offers']->put($offer['offer_id'], [
                "offer_id" => $offer['offer_id'],
                "product_id" => $offer['product_id'],
                "category_id" => $offer['category_id'],
                "brand_id" => $offer['brand_id'] ?? null,
                "price" => $offer['price'],
                "qty" => $offer['qty'] ?? 1,
            ]);
        }

        return $this;
    }

    /**
     * @param string|null $promoCode
     *
     * @return CalculatorBuilder
     */
    public function promoCode(?string $promoCode): CalculatorBuilder
    {
        $this->params['promoCode'] = $promoCode;

        return $this;
    }

    /**
     * @param Collection|array $deliveries
     *
     * @return CalculatorBuilder
     */
    public function deliveries($deliveries): CalculatorBuilder
    {
        $this->params['deliveries'] = collect($deliveries);

        return $this;
    }

    /**
     * @param Collection|array $payment
     *
     * @return CalculatorBuilder
     */
    public function payment($payment): CalculatorBuilder
    {
        $this->params['payment'] = collect($payment);

        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
