<?php

namespace App\Domain\PromoCodes\Models;

use App\Domain\Discounts\Models\Discount;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Промокод"
 *
 * @property int $creator_id - ид создателя
 * @property int|null $seller_id - ид продавца
 * @property int|null $owner_id - ид владельца
 * @property string $name - название
 * @property string $code - код
 * @property int|null $counter - счётчик
 * @property Carbon|null $start_date - дата начала действия промокода
 * @property Carbon|null $end_date - дата окончания действия промокода
 * @property int $status - статус
 * @property int $type - тип
 * @property int|null $discount_id - ид скидки
 * @property array $conditions - условия использования промокода
 * @property Carbon $created_at - дата создания промокода
 */
class PromoCode extends Model
{
    /**
     * Статус промокода
     */
    /** Создана */
    const STATUS_CREATED = 1;
    /** Отправлена на согласование */
    const STATUS_SENT = 2;
    /** На согласовании */
    const STATUS_ON_CHECKING = 3;
    /** Активна */
    const STATUS_ACTIVE = 4;
    /** Отклонена */
    const STATUS_REJECTED = 5;
    /** Приостановлена */
    const STATUS_PAUSED = 6;
    /** Завершена */
    const STATUS_EXPIRED = 7;
    /** Тестовый */
    const STATUS_TEST = 8;

    /**
     * Тип промокода (на что промокод)
     */
    /** Промокод на скидку */
    const TYPE_DISCOUNT = 1;
    /** Промокод на бесплатную доставку */
    const TYPE_DELIVERY = 2;

    /**
     * Тип условия для применения промокода
     */
    /** Для определенного(ых) пользователя(ей) */
    const CONDITION_TYPE_CUSTOMER_IDS = 'customers';
    /** Для определенного(ых) сегмента(ов) */
    const CONDITION_TYPE_SEGMENT_IDS = 'segments';

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'creator_id',
        'seller_id',
        'owner_id',
        'name',
        'code',
        'counter',
        'start_date',
        'end_date',
        'status',
        'type',
        'discount_id',
        'conditions',
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    /**
     * @var array
     */
    protected $casts = [
        'conditions' => 'array',
    ];

    /**
     * Типы промокодов
     * @return array
     */
    public static function availableTypes()
    {
        return [
            self::TYPE_DISCOUNT,
            self::TYPE_DELIVERY,
        ];
    }

    /**
     * @return array
     */
    public static function availableTypesForSeller()
    {
        return [
            self::TYPE_DISCOUNT,
        ];
    }

    /**
     * @return BelongsTo
     */
    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * @param array $customerIds
     * @return PromoCode
     */
    public function setCustomerIds(array $customerIds)
    {
        $conditions = $this->conditions ?? [];
        $conditions[self::CONDITION_TYPE_CUSTOMER_IDS] = $customerIds;
        $this->conditions = $conditions;

        return $this;
    }

    /**
     * @param array $segmentIds
     * @return PromoCode
     */
    public function setSegmentIds(array $segmentIds)
    {
        $conditions = $this->conditions ?? [];
        $conditions[self::CONDITION_TYPE_SEGMENT_IDS] = $segmentIds;
        $this->conditions = $conditions;

        return $this;
    }

    /**
     * @return array
     */
    public function getCustomerIds()
    {
        return $this->conditions[self::CONDITION_TYPE_CUSTOMER_IDS] ?? [];
    }

    /**
     * @return array
     */
    public function getSegmentIds()
    {
        return $this->conditions[self::CONDITION_TYPE_SEGMENT_IDS] ?? [];
    }

    /**
     * Является ли промокод персональным (личный промокод РП)
     * @return bool
     */
    public function isPersonal()
    {
        return $this->owner_id > 0;
    }

    /**
     * Активные и доступные на заданную дату скидки
     *
     * @param Builder $query
     * @param Carbon|null $date
     * @return Builder
     */
    public function scopeActive(Builder $query, ?Carbon $date = null): Builder
    {
        $date = $date ?? Carbon::now()->startOfDay();

        return $query
            ->whereIn('status', [self::STATUS_ACTIVE, self::STATUS_TEST])
            ->where(function ($query) use ($date) {
                $query->where('start_date', '<=', $date)->orWhereNull('start_date');
            })->where(function ($query) use ($date) {
                $query->where('end_date', '>=', $date)->orWhereNull('end_date');
            });
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }

    public function scopeIsUnlimited(Builder $query, bool $value): Builder
    {
        return $query->where(function ($query) use ($value) {
            $query->whereNull('start_date', not: !$value)
                ->whereNull('end_date', not: !$value);
        });
    }

    public static function boot()
    {
        parent::boot();

        self::saved(function (self $item) {
            /**
             * Скидка доступна только по промокоду
             */
            if ($item->discount_id) {
                /** @var Discount $discount */
                $discount = Discount::find($item->discount_id);
                if ($discount && !$discount->promo_code_only) {
                    $discount->promo_code_only = true;
                    $discount->save();
                }
            }
        });
    }
}
