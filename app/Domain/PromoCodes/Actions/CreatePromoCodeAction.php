<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;
use Illuminate\Support\Arr;

/**
 * Class CreatePromoCodeAction
 * @package App\Domain\PromoCodes\Actions
 */
class CreatePromoCodeAction
{
    public function execute(array $fields): PromoCode
    {
        return PromoCode::create(Arr::only($fields, PromoCode::FILLABLE));
    }
}
