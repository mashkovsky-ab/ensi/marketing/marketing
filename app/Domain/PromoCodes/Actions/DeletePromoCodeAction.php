<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;

/**
 * Class DeletePromoCodeAction
 * @package App\Domain\PromoCodes\Actions
 */
class DeletePromoCodeAction
{
    public function execute(int $promoCodeId): void
    {
        PromoCode::destroy($promoCodeId);
    }
}
