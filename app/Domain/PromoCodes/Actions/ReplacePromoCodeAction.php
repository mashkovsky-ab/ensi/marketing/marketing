<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;
use Illuminate\Support\Arr;

/**
 * Class ReplacePromoCodeAction
 * @package App\Domain\PromoCodes\Actions
 */
class ReplacePromoCodeAction
{
    public function execute(int $promoCodeId, array $fields): PromoCode
    {
        $promoCode = PromoCode::findOrFail($promoCodeId);

        $newAttributes = Arr::only($fields, PromoCode::FILLABLE);
        foreach (PromoCode::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $promoCode->update($newAttributes);

        return $promoCode;
    }
}
