<?php

namespace App\Domain\PromoCodes\Actions;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Domain\PromoCodes\Validators\PromoCodeTypeValidator;
use Illuminate\Support\Arr;

/**
 * Class PatchPromoCodeAction
 * @package App\Domain\PromoCodes\Actions
 */
class PatchPromoCodeAction
{
    public function execute(int $promoCodeId, array $fields): PromoCode
    {
        /** @var PromoCode $promoCode */
        $promoCode = PromoCode::findOrFail($promoCodeId);
        PromoCodeTypeValidator::validate($promoCode, $fields);
        $promoCode->update(Arr::only($fields, PromoCode::FILLABLE));

        return $promoCode;
    }
}
