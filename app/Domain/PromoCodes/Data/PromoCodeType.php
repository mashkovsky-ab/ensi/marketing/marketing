<?php


namespace App\Domain\PromoCodes\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeTypeEnum;

class PromoCodeType
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            PromoCodeTypeEnum::TYPE_DISCOUNT => 'На скидку',
            PromoCodeTypeEnum::TYPE_DELIVERY => 'На бесплатную доставку',
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (PromoCodeTypeEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
