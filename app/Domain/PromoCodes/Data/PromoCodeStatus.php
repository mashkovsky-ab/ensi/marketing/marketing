<?php


namespace App\Domain\PromoCodes\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeStatusEnum;

class PromoCodeStatus
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            PromoCodeStatusEnum::STATUS_CREATED => 'Создан',
            PromoCodeStatusEnum::STATUS_SENT => 'Отправлен на согласование',
            PromoCodeStatusEnum::STATUS_ON_CHECKING => 'На согласовании',
            PromoCodeStatusEnum::STATUS_ACTIVE => 'Активный',
            PromoCodeStatusEnum::STATUS_REJECTED => 'Отклонен',
            PromoCodeStatusEnum::STATUS_PAUSED => 'Приостановлен',
            PromoCodeStatusEnum::STATUS_EXPIRED => 'Завершен',
            PromoCodeStatusEnum::STATUS_TEST => 'Тестовый',
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (PromoCodeStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
