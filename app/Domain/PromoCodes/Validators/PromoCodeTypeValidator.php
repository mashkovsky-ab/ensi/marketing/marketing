<?php

namespace App\Domain\PromoCodes\Validators;

use App\Domain\PromoCodes\Models\PromoCode;
use App\Http\ApiV1\OpenApiGenerated\Enums\PromoCodeTypeEnum;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class PromoCodeTypeValidator
 * @package App\Domain\PromoCodes\Validators
 */
class PromoCodeTypeValidator
{
    public static function validate(PromoCode $promoCode, array $fields)
    {
        $fields['type'] = $promoCode->type;
        $fields['discount_id'] = $fields['discount_id'] ?? $promoCode->discount_id;

        Validator::validate($fields, [
            'type' => ['required', 'integer', Rule::in(PromoCodeTypeEnum::cases())],
            'discount_id' => [
                'required_if:type,' . PromoCodeTypeEnum::TYPE_DISCOUNT,
                'prohibited_unless:type,' . PromoCodeTypeEnum::TYPE_DISCOUNT,
            ],
        ]);
    }
}
