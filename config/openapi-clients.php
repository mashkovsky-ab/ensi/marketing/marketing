<?php

return [
   'catalog' => [
      'pim' => [
         'base_uri' => env('PIM_HOST') . "/api/v1",
      ],
   ],
   'orders' => [
      'oms' => [
         'base_uri' => env('OMS_HOST') . "/api/v1",
      ],
   ],
];
