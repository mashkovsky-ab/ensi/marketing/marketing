<?php

return [
    'price' => env('PRICE_PRECISION', 2),
];
